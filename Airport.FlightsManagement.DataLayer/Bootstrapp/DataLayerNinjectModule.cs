﻿using Ninject.Modules;

namespace Airport.FlightsManagement.DataLayer.Bootstrapp
{
    public class DataLayerNinjectModule:NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IAirportFlightsManagementDbContext>().ToMethod(x => new AirportFlightsManagementDbContext());
        }
    }
}
