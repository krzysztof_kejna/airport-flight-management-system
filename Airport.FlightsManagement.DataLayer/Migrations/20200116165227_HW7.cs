﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Airport.FlightsManagement.DataLayer.Migrations
{
    public partial class HW7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "FlightDurration",
                table: "FlightPaths",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FlightDurration",
                table: "FlightPaths");
        }
    }
}
