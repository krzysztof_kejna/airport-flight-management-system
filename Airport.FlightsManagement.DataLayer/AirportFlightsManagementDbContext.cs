﻿using Microsoft.EntityFrameworkCore;
using Airport.FlightsManagement.DataLayer.Models;
using System;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Threading.Tasks;

namespace Airport.FlightsManagement.DataLayer
{
    public interface IAirportFlightsManagementDbContext:IDisposable
    {
        DbSet<Airline> Airlines { get; set; }
        DbSet<Plane> Planes { get; set; }
        DbSet<Port> Ports { get; set; }
        DbSet<FlightPath> FlightPaths { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<Ticket> Tickets { get; set; }
        DatabaseFacade Database { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }

    public class AirportFlightsManagementDbContext : DbContext, IAirportFlightsManagementDbContext
    {
        private const string _connectionString = "Data Source=.;Initial Catalog=AirportFlightsManagementDb;Integrated Security=True;";
        public DbSet<Plane> Planes { get; set; }
        public DbSet<Port> Ports { get; set; }
        public DbSet<Airline> Airlines { get; set; }
        public DbSet<FlightPath> FlightPaths { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Ticket> Tickets { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
