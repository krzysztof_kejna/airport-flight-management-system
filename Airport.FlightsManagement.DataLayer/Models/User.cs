﻿using System;

namespace Airport.FlightsManagement.DataLayer.Models
{
    public class User:Entity
    {
        public string Login { get; set; }
        public string PasswordHash { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime? BirthDate { get; set; }
        public double DistanceTraveled { get; set; }
    }
}
