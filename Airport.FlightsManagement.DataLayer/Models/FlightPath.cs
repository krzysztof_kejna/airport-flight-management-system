﻿using System;

namespace Airport.FlightsManagement.DataLayer.Models
{
    public class FlightPath : Entity
    {
        public int PlaneId { get; set; }
        public Plane Plane { get; set; }
        public int AirlineId { get; set; }
        public Airline Airline { get; set; }
        public int PortId { get; set; }
        public Port Port { get; set; }
        public string PathCode { get; set; }
        public int FlightDay { get; set; }
        public DateTime? FlightTime { get; set; }
        public double FlightDurration { get; set; }
        public double Cost { get; set; }
    }
}
