﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.DataLayer.Models
{
    public class Ticket : Entity
    {
        public int FlightPathId { get; set; }
        public FlightPath FlightPath { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime? FlightDate { get; set; }
        public bool Active { get; set; }
        public double Cost { get; set; }
    }
}
