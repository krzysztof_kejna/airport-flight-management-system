﻿namespace Airport.FlightsManagement.DataLayer.Models
{
    public class Port : Entity
    {
        public string Name { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
