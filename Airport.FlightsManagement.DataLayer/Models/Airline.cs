﻿namespace Airport.FlightsManagement.DataLayer.Models
{
    public class Airline : Entity
    {
        public string Name { get; set; }
        public string LineCode { get; set; }
    }
}
