﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.DataLayer.Models
{
    public class Plane : Entity
    {
        public string Manifacturer { get; set; }
        public string Model { get; set; }
        public int SeatCount { get; set; }
    }
}
