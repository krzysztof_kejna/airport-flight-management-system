﻿using Airport.FlightsManagement.BusinessLayer.Services;
using Airport.FlightsManagement.DataLayer.Models;
using Airport.FlightsManagement.WebApi.Bootstrapp;
using Airport.FlightsManagement.WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Ninject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Airport.FlightsManagement.WebApi.Controllers
{
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private IUserService _usersService;

        public UsersController()
        {
            var kernel = DependencyResolver.GetKernel();

            _usersService = kernel.Get<IUserService>();
        }

        /*
        Method: POST
        URI: http://localhost:10500/api/users
        Body:
        {
            "name": "Jan",
            "surname": "Nush",
            "birthDate": "2020-01-20T10:35:19.949Z",
            "distanceTraveled": 0,
            "id": 0
        }
        */
        [HttpPost]
        public async Task<int> PostUser([FromBody]User newUser)
        {
            var userId = await _usersService.AddUser(newUser);
            return userId;
        }

        [HttpGet ("{login}")]
        public async Task<User> GetUserByLogin(string login)
        {
            var user = await _usersService.GetUserByLogin(login);
            user.PasswordHash = "";
            return user;
        }

        [HttpGet("logout")]
        public async Task<bool> UserLogout()
        {
            return false;
        }

        [HttpGet("avalible/{login}")]
        public async Task<bool> CheckLoginAvalibility(string login)
        {
            var avaliblity = await _usersService.CheckLoginAvalibility(login);
            return avaliblity;
        }

        [HttpPost("auth")]
        public async Task<bool> PostAuthenticateUser([FromBody] UserAuthenticationData userData)
        {
            return await _usersService.AuthenticateUser(userData.UserName, userData.Password);
        }
    }
}
