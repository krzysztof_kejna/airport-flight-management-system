﻿using Airport.FlightsManagement.BusinessLayer.Models;
using Airport.FlightsManagement.BusinessLayer.Services;
using Airport.FlightsManagement.DataLayer.Models;
using Airport.FlightsManagement.WebApi.Bootstrapp;
using Microsoft.AspNetCore.Mvc;
using Ninject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Airport.FlightsManagement.WebApi.Controllers
{
    [Route("api/tickets")]
    public class TicketController : ControllerBase
    {
        private ITicketService _ticketService;

        public TicketController()
        {
            var kernel = DependencyResolver.GetKernel();

            _ticketService = kernel.Get<ITicketService>();
        }

    /*
        Method: POST
        URI: http://localhost:10500/api/tickets
        Body:
        {
          "flightPathId": 1,

          "userId": 1,

          "flightDate": "2020-01-20T10:49:18.858Z",
          "active": true,
          "cost": 22,
          "id": 0
        }
    */
        [HttpPost]
        public async Task<int> BuyTicket([FromBody]Ticket ticket)
        {
            var ticketId = await _ticketService.AddTicket(ticket.UserId, ticket.FlightPathId, ticket);
            return ticketId;
        }

        /*
            Method: GET
            URI: http://localhost:10500/api/tickets/bought
            Body: no body
        */
        [HttpGet("bought")]
        public async Task<List<CreatedTickets>> GetAllBoughtTickets()
        {
            var result = await _ticketService.GetAllNotCanceledTickets();
            return result;
        }

        [HttpGet("bought/{id}")]
        public async Task<List<CreatedTickets>> GetUserAllTickets(int id)
        {
            var result = await _ticketService.GetUserAllTickets(id);
            return result;
        }
    }
}