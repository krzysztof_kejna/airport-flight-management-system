﻿using Airport.FlightsManagement.BusinessLayer.Models;
using Airport.FlightsManagement.BusinessLayer.Services;
using Airport.FlightsManagement.DataLayer.Models;
using Airport.FlightsManagement.WebApi.Bootstrapp;
using Microsoft.AspNetCore.Mvc;
using Ninject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Airport.FlightsManagement.WebApi.Controllers
{
    [Route("api/flights")]
    public class FlightPathsController : ControllerBase
    {
        private IFlightPathService _flightPathService;

        public FlightPathsController()
        {
            var kernel = DependencyResolver.GetKernel();

            _flightPathService = kernel.Get<IFlightPathService>();
        }

        /*
            Method: GET
            URI: http://localhost:10500/api/flights
            Body: no body
        */
        [HttpGet()]
        public async Task<List<CreatedPathFlights>> GetAllFlightPaths()
        {
            var result = await _flightPathService.GetAllFlightPaths();
            return result;
        }

        [HttpGet("{id}")]
        public async Task<FlightPath> GetFlightPath(int id)
        {
            var result = await _flightPathService.GetFlightPathById(id);
            return result;
        }
    }
}
