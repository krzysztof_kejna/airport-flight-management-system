﻿using Airport.FlightsManagement.BusinessLayer.Models;
using Airport.FlightsManagement.BusinessLayer.Services;
using Airport.FlightsManagement.WebApi.Bootstrapp;
using Microsoft.AspNetCore.Mvc;
using Ninject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Airport.FlightsManagement.WebApi.Controllers
{
    [Route("api/board")]
    public class FlightBoardController : ControllerBase
    {
        private IFlightBoardService _flightBoardService;

        public FlightBoardController()
        {
            var kernel = DependencyResolver.GetKernel();

            _flightBoardService = kernel.Get<IFlightBoardService>();
        }

        /*
        Method: GET
        URI: http://localhost:10500/api/board/departured
        Body: no body
        */
        [HttpGet("departured")]
        public async Task<List<Flight>> GetAllFlightDepartured()
        {
            var result = await _flightBoardService.GetFlightData();
            return result;
        }

        /*
        Method: GET
        URI: http://localhost:10500/api/board/arrived
        Body: no body
        */
        [HttpGet("arrived")]
        public async Task<List<Flight>> GetallFlightArrived()
        {
            var result = await _flightBoardService.GetNext20Arrivals();
            return result;
        }
    }
}
