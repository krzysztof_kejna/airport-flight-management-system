﻿using Airport.FlightsManagement.WebApi.Bootstrapp;
using Microsoft.AspNetCore.Hosting;
using System;
using Topshelf;

namespace Airport.FlightsManagement.WebApi
{
    internal class Program
    {
        static void Main()
        {
            var host = HostFactory.Run(config =>
            {
                config.Service<AppHost>(s =>
                {
                    s.ConstructUsing(_ => new AppHost());
                    s.WhenStarted(appHost => appHost.Start());
                    s.WhenStopped(appHost => appHost.Stop());
                });

                config.RunAsLocalSystem();

                config.SetServiceName(TextConstans.ServiceName + "1");
                config.SetDisplayName(TextConstans.ServiceName + "1");
                config.SetDescription(TextConstans.ServiceDescription);
            });

            var exitCode = (int)Convert.ChangeType(host, host.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }
}
