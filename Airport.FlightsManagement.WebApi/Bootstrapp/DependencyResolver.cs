﻿using Airport.FlightsManagement.BusinessLayer.Bootstrapp;
using Airport.FlightsManagement.DataLayer.Bootstrapp;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.WebApi.Bootstrapp
{
    public static class DependencyResolver
    {
        private static IKernel _kernel = null;

        public static IKernel GetKernel()
        {
            if (_kernel != null)
            {
                return _kernel;
            }

            _kernel = new StandardKernel();

            _kernel.Load(new INinjectModule[]
            {
                new BusinessLayerNinjectModule(),
                new DataLayerNinjectModule()
            });

            return _kernel;
        }
    }
}
