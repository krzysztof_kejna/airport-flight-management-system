﻿using Airport.FlightsManagement.BusinessLayer.Services;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Ninject;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.IO;
using System.Threading.Tasks;

namespace Airport.FlightsManagement.WebApi.Bootstrapp
{
    class AppHost
    {
        private IWebHost _webHost;
        private IEventStoreConnectionService _eventStoreConnectionService;
        public AppHost()
        {
            var kernel = DependencyResolver.GetKernel();

            _eventStoreConnectionService = kernel.Get<IEventStoreConnectionService>();
        }

        public void Start()
        {
            InitializeEventStore();

            _webHost = WebHost.CreateDefaultBuilder()
                .ConfigureLogging(ConfigureLogging)
               .ConfigureServices(services =>
               {
                   services.AddMvc();
                   services.AddCors(options =>
                   {
                       options.AddPolicy("AllowAll",
                           builder =>
                           {
                               builder
                                   .AllowAnyOrigin()
                                   .AllowAnyMethod()
                                   .AllowAnyHeader()
                                   .AllowCredentials();
                           });
                   });
                   services.AddSwaggerGen(SwaggerDocsConfig);
               })
               .Configure(app =>
               {
                   app.UseCors("AllowAll");
                   app.UseMvc();
                   app.UseSwagger();
                   app.UseSwaggerUI(c =>
                   {
                       c.SwaggerEndpoint("/swagger/v1/swagger.json", TextConstans.ServiceName);
                       c.RoutePrefix = string.Empty;
                   });
               })
               .UseUrls("http://*:10500")
               .Build();

            _webHost.Start();
        }

        private async Task InitializeEventStore()
        {
            _eventStoreConnectionService.Connect();
            await _eventStoreConnectionService.SendFlightBoardEventStore();
            _eventStoreConnectionService.Register();
            _eventStoreConnectionService.SubscribeForArrivals();
        }

        private static void ConfigureLogging(WebHostBuilderContext hostingContext, ILoggingBuilder logging)
        {
            logging.ClearProviders();
        }

        private void SwaggerDocsConfig(SwaggerGenOptions genOptions)
        {
            genOptions.SwaggerDoc(
                "v1",
                new Info
                {
                    Version = "v1",
                    Title = TextConstans.ServiceName,
                    Description = TextConstans.ServiceDescription,
                    TermsOfService = "https://webapiexamples.project.com/terms",
                    Contact = new Contact
                    {
                        Name = "Krzysztof Kejna",
                    },
                    License = new License
                    {
                    }
                });


            var xmlFile = Path.ChangeExtension(typeof(Program).Assembly.Location, ".xml");
            genOptions.IncludeXmlComments(xmlFile);
        }

        public void Stop()
        {
            _eventStoreConnectionService.CloseConnection();
            _webHost.StopAsync().Wait();
            _webHost.Dispose();
        }    
    }
}
