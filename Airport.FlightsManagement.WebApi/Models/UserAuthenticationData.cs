﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.WebApi.Models
{
    public class UserAuthenticationData
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
