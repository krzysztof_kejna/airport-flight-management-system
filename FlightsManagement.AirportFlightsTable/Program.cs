﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Airport.FlightsManagement.BusinessLayer.Services;
using EventStore.ClientAPI;
using Newtonsoft.Json;

namespace FlightsManagement.AirportFlightsTable
{
    class Program
    {
        private const string stream = "flightmanagement-departures-stream";
        private const int defaultPort = 1113;

        static void Main(string[] args)
        {
            new Program().Run();
        }

        public void Run()
        {
            GetLastMessage();
            Subscribe();
        }

        private void GetLastMessage()
        {
            var settings = ConnectionSettings.Create();

            using (var conn = EventStoreConnection.Create(settings, new IPEndPoint(IPAddress.Loopback, defaultPort)))
            {
                conn.ConnectAsync().Wait();

                var currentSlice = conn.ReadStreamEventsBackwardAsync(stream, (long)StreamPosition.End, 20, false).Result;
                var latestMessage = currentSlice.Events;
                foreach (var message in latestMessage)
                {
                    var data = Encoding.ASCII.GetString(message.Event.Data);
                    var flightList = JsonConvert.DeserializeObject<List<Flight>>(data);

                    Console.Clear();
                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine($"System time:{SystemTimeService.Now()}, Real time:{DateTime.Now}");

                    var flights = flightList.OrderBy(f => f.DepartureDate)
                    .Take(20)
                    .ToList();
                    foreach (var flight in flights)
                    {
                        Console.WriteLine($"{flight.FlightCode}, {flight.DepartureDate}, {flight.DepartureStatus},  {flight.ArrivalAirportName}");
                    }
                    Thread.Sleep(1000);
                }
            }
        }

        private void Subscribe()
        {
            var settings = ConnectionSettings.Create();

            using (var conn = EventStoreConnection.Create(settings, new IPEndPoint(IPAddress.Loopback, defaultPort)))
            {
                conn.ConnectAsync().Wait();
                var sub = conn.SubscribeToStreamAsync(stream, true,
                (_, x) =>
                {
                    var data = Encoding.ASCII.GetString(x.Event.Data);
                    var flightList = JsonConvert.DeserializeObject<List<Flight>>(data);

                    Console.Clear();
                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine($"System time:{SystemTimeService.Now()}, Real time:{DateTime.Now}");

                    var flights = flightList.OrderBy(f => f.DepartureDate)
                                    .Take(20)
                                    .ToList();
                    foreach (var flight in flights)
                    {
                        Console.WriteLine($"{flight.FlightCode}, {flight.DepartureDate}, {flight.DepartureStatus},  {flight.ArrivalAirportName}");
                    }
                });
                Console.ReadLine();
            }
        }
    }
}


