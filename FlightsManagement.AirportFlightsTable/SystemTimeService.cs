﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightsManagement.AirportFlightsTable

{ 
    public class SystemTimeService
    {
        public static DateTime Now()
        {
            return TimeBendCalculator(DateTime.Now).Value;
        }

        public static DateTime? TimeBendCalculator(DateTime? typedTime)
        {
            var baseDate = new DateTime(2019, 09, 30);
            var baseDateTicks = baseDate.Ticks;
            long ticks = new DateTime(typedTime.Value.Year, typedTime.Value.Month, typedTime.Value.Day, typedTime.Value.Hour, typedTime.Value.Minute, typedTime.Value.Second).Ticks;
            ticks = (ticks - baseDateTicks) * 60;
            var date = baseDate.AddTicks(ticks);
            return date;
        }
    }
}
