﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.Cli.Helpers
{
    internal interface IMenu
    {
        void AddMenuItem(Action action, string description, int key);
        void ExecuteAction(int key);
        void ShowMenuItems();
    }

    internal class Menu : IMenu
    {
        private Dictionary<int, MenuItem> _menuItems = new Dictionary<int, MenuItem>();

        public void AddMenuItem(Action action, string description, int key)
        {
            if (_menuItems.ContainsKey(key))
            {
                throw new Exception($"There is already a menu item with key: {key}");
            }

            _menuItems.Add(
                key, new MenuItem { Action = action, Description = description });
        }

        public void ExecuteAction(int key)
        {
            if (!_menuItems.ContainsKey(key))
            {
                throw new Exception($"There is no menu item with key:{key}");
            }
            _menuItems[key].Action();
        }

        public void ShowMenuItems()
        {
            Console.WriteLine("Available menu commands:");
            foreach (var menuItem in _menuItems)
            {
                Console.WriteLine($"\t{menuItem.Key} {menuItem.Value.Description}");
            }
        }
    }
}
