﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.Cli.Helpers
{
    internal class MenuItem
    {
        public string Description;
        public Action Action;
    }
}
