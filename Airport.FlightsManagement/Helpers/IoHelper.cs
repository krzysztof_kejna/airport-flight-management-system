﻿using Airport.FlightsManagement.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Airport.FlightsManagement.Cli.Helpers
{
    internal interface IIoHelper
    {
        int GetIntFromUser(string message);
        double GetDoubleFromUser(string message);
        string GetStringFromUser(string message);
        DateTime? GetDateFromUser(string message, string dateFormat);
        string CheckForAValidDateTime(DateTime? time);
        string GetDayOfTheWeek(int dayNumber);
        double GetPriceFromUser(string message);
        void PrintAndClear(string message);
    }

    internal class IoHelper : IIoHelper
    {
        public int GetIntFromUser(string message)
        {
            int result = 0;
            Console.WriteLine(message);
            while (!int.TryParse(Console.ReadLine(), out result))
            {
                Console.WriteLine("It was not a number, Try again");
            }
            return result;
        }

        public double GetDoubleFromUser(string message)
        {
            Console.WriteLine(message);
            double result;
            string replace = Console.ReadLine();
            replace = replace.Replace(",", ".");
            while (!double.TryParse(replace, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                Console.WriteLine("Wrong input, please type number");
                replace = Console.ReadLine();
                replace = replace.Replace(",", ".");
            }
            return result;
        }

        public string GetStringFromUser(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }

        public DateTime? GetDateFromUser(string message, string dateFormat)
        {
            var result = new DateTime();
            var success = false;

            while (!success)
            {
                Console.Write($"{message} ({dateFormat}): ");
                var input = Console.ReadLine();
                if(input == "")
                {
                    return null;
                }

                success = DateTime.TryParseExact(
                    input,
                    dateFormat,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out result);

                if (!success)
                {
                    Console.WriteLine("It was not a date in a correct format. Try again...");
                }
            }

            return result;
        }

        public string CheckForAValidDateTime(DateTime? time)
        {
            string data;
            if (time.HasValue)
            {
                data = time.Value.ToString("d");
            }
            else
            {
                data = "empty";
            }

            return data;
        }

        public string GetDayOfTheWeek(int dayNumber)
        {
            return Enum.GetName(typeof(DayOfWeek), dayNumber - 1);
        }

        public double GetPriceFromUser(string message)
        {
            double cost = GetDoubleFromUser(message);
            double check = cost;

            while (check * 100 % 1 >= double.Epsilon)
            {
                Console.WriteLine("Wrong price, please try again");
                cost = GetDoubleFromUser("Price");
                check = cost * 100 % 1;
            }

            return cost;
        }

        public void PrintAndClear(string message)
        {
            Console.WriteLine(message);
            Console.ReadLine();
            Console.Clear();
        }
    }
}
