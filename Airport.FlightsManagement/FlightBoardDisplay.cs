﻿using Airport.FlightsManagement.BusinessLayer.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Airport.FlightsManagement.BusinessLayer.Models;
using EventStore.ClientAPI;
using EventStore.ClientAPI.SystemData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace Airport.FlightsManagement.Cli
{
    internal interface IFlightBoardDisplay
    {
        void Start();
        void Stop();
        void DisplayArrivalFlightBoard();

    }

    class FlightBoardDisplay : IFlightBoardDisplay
    {
        private readonly IFlightBoardService _flightBoardService;
        private readonly ISystemTimeService _systemTimeService;
        private Task _queueMonitoringTask = null;
        private CancellationTokenSource _queueMonitoringTaskCts = null;


        public FlightBoardDisplay(
            IFlightBoardService flightBoardService,
            ISystemTimeService systemTimeService)
        {
            _flightBoardService = flightBoardService;
            _systemTimeService = systemTimeService;
        }

        public void Start()
        {
            if (_queueMonitoringTask != null)
            {
                return;
            }

            _queueMonitoringTaskCts = new CancellationTokenSource();
            var token = _queueMonitoringTaskCts.Token;

            _queueMonitoringTask = new Task(() => MonitorQueue(token), token);
            _queueMonitoringTask.Start();
        }

        public void Stop()
        {
            if (_queueMonitoringTask == null)
            {
                return;
            }

            _queueMonitoringTaskCts.Cancel();
            _queueMonitoringTask.Wait();
            _queueMonitoringTask.Dispose();
            _queueMonitoringTask = null;
        }

        private void MonitorQueue(CancellationToken ct)
        {
            while (true)
            {
                if (ct.IsCancellationRequested)
                {
                    return;
                }
                Console.Clear();
                Console.SetCursorPosition(0, 0);
                Console.WriteLine($"System time:{_systemTimeService.Now()}, Real time:{DateTime.Now}");
                foreach (var flight in _flightBoardService.GetFlightData().Result)
                {
                    Console.WriteLine($"{flight.FlightCode}, {flight.ArrivalAirportName}, {flight.DepartureDate}, {flight.DepartureStatus}");
                }
                Console.WriteLine("Press enter to return to menu");
                Thread.Sleep(1000);
            }
        }

        public void DisplayArrivalFlightBoard()
        {
            bool working = true;

            var task = new Task(() =>
            {
                while (working)
                {
                    Console.Clear();
                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine($"System time:{_systemTimeService.Now()}, Real time:{DateTime.Now}");
                    var arrivals = _flightBoardService.GetNext20Arrivals();

                    foreach (var result in arrivals.Result)
                    {
                        Console.WriteLine($"{result.FlightCode}, {result.DepartureAirportName}, {result.DepartureDate}, {result.ArrivalAirportName}, {result.ArrivalDate}, {result.ArrivalStatus}");
                    }

                    Thread.Sleep(1000);
                }
            });

            task.Start();
            Console.ReadLine();
            working = false;
            Console.Clear();
        }


    }
}


    

