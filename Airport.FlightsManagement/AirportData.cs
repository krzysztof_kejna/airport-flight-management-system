﻿namespace Airport.FlightsManagement.Cli
{
    public class AirportData
    {
        public const string AirportName = "KrzysiekAirport";
        public const string CityName = "Gdańsk";
        public const string CountryName = "Polska";
        public const double Latitude = 54.22;
        public const double Longitude = 18.38;
    }
}
