﻿using Airport.FlightsManagement.Cli.Helpers;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.Cli.Bootstrapp
{
    public class CliNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IProgram>().To<Program>();
            Kernel.Bind<IMenu>().To<Menu>();
            Kernel.Bind<IIoHelper>().To<IoHelper>();
            Kernel.Bind<IFlightBoardDisplay>().To<FlightBoardDisplay>();
        }
    }
}
