﻿using Airport.FlightsManagement.BusinessLayer.Bootstrapp;
using Airport.FlightsManagement.DataLayer.Bootstrapp;
using Ninject;
using Ninject.Modules;

namespace Airport.FlightsManagement.Cli.Bootstrapp
{
    public static class DependencyResolver
    {
        public static IKernel GetKernel()
        {
            var kernel = new StandardKernel();

            kernel.Load(new INinjectModule[]
            {
                new CliNinjectModule(),
                new DataLayerNinjectModule(),
                new BusinessLayerNinjectModule()
            });

            return kernel;
        }
    }
}
