﻿using Airport.FlightsManagement.BusinessLayer.Bootstrapp;
using Airport.FlightsManagement.BusinessLayer.Models;
using Airport.FlightsManagement.BusinessLayer.Services;
using Airport.FlightsManagement.Cli.Bootstrapp;
using Airport.FlightsManagement.Cli.Helpers;
using Airport.FlightsManagement.DataLayer.Models;
using EventStore.ClientAPI;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Airport.FlightsManagement.Cli
{
    public interface IProgram
    {
        void Run();
    }

    internal class Program : IProgram
    {
        private List<Flight> _arrivalList;
        private readonly IIoHelper _ioHelper;
        private readonly IMenu _menu;
        private readonly IDatabaseInitializer _databaseInitializer;

        private readonly IPortService _portService;
        private readonly IAirlineService _airlineService;
        private readonly IPlaneService _planeService;
        private readonly IFlightPathService _flightPathService;
        private readonly IUserService _userService;
        private readonly IGeoLocationService _geoLoactionService;
        private readonly ITicketCostCalculationService _ticketCostCalculationService;
        private readonly ITicketService _ticketService;
        private readonly IFlightBoardService _flightBoardService;
        private readonly IFlightBoardDisplay _flightBoardDisplay;
        private readonly IJsonConversionService _jsonConversionService;
        private readonly IDistanceCalculator _distanceCalculator;
        private readonly ICryptographyService _cryptographyService;
        private readonly IEventStoreConnectionService _eventStoreConnectionService;

        static void Main(string[] args)
        {
            using (var kernel = DependencyResolver.GetKernel())
            {
                var program = kernel.Get<IProgram>();
                program.Run();
            }
        }

        public Program(IMenu menu,
            IIoHelper ioHelper,
            IDatabaseInitializer databaseInitializer,
            IPortService portService,
            IAirlineService airlineService,
            IPlaneService planeService,
            IFlightPathService flightPathService,
            IUserService userService,
            IGeoLocationService geoLoactionService,
            ITicketCostCalculationService ticketCostCalculationService,
            ITicketService ticketService,
            IFlightBoardService flightBoardService,
            IFlightBoardDisplay flightBoardDisplay,
            IJsonConversionService jsonConversionService,
            IDistanceCalculator distanceCalculator,
            ICryptographyService cryptographyService,
            IEventStoreConnectionService eventStoreConnectionService)
        {
            _menu = menu;
            _ioHelper = ioHelper;
            _databaseInitializer = databaseInitializer;
            _portService = portService;
            _airlineService = airlineService;
            _planeService = planeService;
            _flightPathService = flightPathService;
            _userService = userService;
            _geoLoactionService = geoLoactionService;
            _ticketCostCalculationService = ticketCostCalculationService;
            _ticketService = ticketService;
            _flightBoardService = flightBoardService;
            _flightBoardDisplay = flightBoardDisplay;
            _jsonConversionService = jsonConversionService;
            _distanceCalculator = distanceCalculator;
            _cryptographyService = cryptographyService;
            _eventStoreConnectionService = eventStoreConnectionService;
        }

        public void Run()
        {
            InitializeMenuItems();
            InitializeDatabase();
            InitializeEventStore();


            while (true)
            {
                _menu.ShowMenuItems();

                var input = _ioHelper.GetIntFromUser("Enter command number");
                Console.Clear();
                try
                {
                    _menu.ExecuteAction(input);
                }
                catch ( Exception e)
                {
                    Console.WriteLine($"Command execution failed {e.Message}");
                }
            }
        }

        private void InitializeDatabase()
        {
            _databaseInitializer.Initialize();
        }

        private void InitializeMenuItems()
        {
            _menu.AddMenuItem(CreatePort, "Create new port", 1);

            _menu.AddMenuItem(CreatePlane, "Create new plane", 2);
            _menu.AddMenuItem(CreateAirline, "Create new airline", 3);
            _menu.AddMenuItem(CreateFlightPath, "Create new flight path", 4);
            _menu.AddMenuItem(CreateUser, "Create new user", 5);
            _menu.AddMenuItem(PortList, "Port list", 6);
            _menu.AddMenuItem(PlaneList, "Plane list", 7);
            _menu.AddMenuItem(AirlineList, "Airline list", 8);
            _menu.AddMenuItem(FlightPathList, "Flight path list", 9);
            _menu.AddMenuItem(UserList, "User list", 10);
            _menu.AddMenuItem(EditUser, "Edit user", 11);
            _menu.AddMenuItem(SellTicket, "Sell ticket", 12);
            _menu.AddMenuItem(CancelTicket, "Cancel ticket", 13);
            _menu.AddMenuItem(TicketHistory, "Ticket history", 14);
            _menu.AddMenuItem(FlightBoard, "Show flight board", 15);
           _menu.AddMenuItem(DisplayArivalBoardData, "Display arival board", 16);
            _menu.AddMenuItem(Exit, "Exit", 18);
        }

        private async Task InitializeEventStore()
        {
            _eventStoreConnectionService.Connect();
            await _eventStoreConnectionService.SendFlightBoardEventStore();
            _eventStoreConnectionService.Register();
            _eventStoreConnectionService.SubscribeForArrivals();
        }

        private void CreatePort()
        {
            Console.WriteLine("Chosen option: 1. Create new port\n");
            var name = _ioHelper.GetStringFromUser("Type port name");
            var cityName = _ioHelper.GetStringFromUser("Type city name");
            var countryName = _ioHelper.GetStringFromUser("Type country name");
            var latitude = _ioHelper.GetDoubleFromUser("Type latitude");
            var longitude = _ioHelper.GetDoubleFromUser("type longitude");

            try
            {
                var coordinates = _geoLoactionService.GetCoordinations(latitude, longitude);
                latitude = coordinates.Latitude;
                longitude = coordinates.Longitude;
            }
            catch(Exception e)
            {
                Console.WriteLine($"Command execution failed {e.Message}");
                return;
            }

            var newPort = new Port
            {
                Name = name,
                CityName = cityName,
                CountryName = countryName,
                Latitude = latitude,
                Longitude = longitude
            };

            var newPortId = _portService.AddPort(newPort);
            Console.WriteLine($"Airport created with ID {newPortId}");
            _ioHelper.PrintAndClear("Press any key to continue");
        }       
        
        private void CreatePlane()
        {
            Console.WriteLine("Chosen option: 2. Create new plane\n");

            var newPlane = new Plane
            {
                Manifacturer = _ioHelper.GetStringFromUser("Type manifacturer name"),
                Model = _ioHelper.GetStringFromUser("Type model"),
                SeatCount = _ioHelper.GetIntFromUser("Type seats counts")
            };

            var newPlaneId = _planeService.AddPlane(newPlane);
            Console.WriteLine($"Plane created with ID {newPlaneId}");
            _ioHelper.PrintAndClear("Press any key to continue");
        }       
        
        private void CreateAirline()
        {
            Console.WriteLine("Chosen option: 3. Create new airline\n");

            var newAirline = new Airline
            {
                Name = _ioHelper.GetStringFromUser("Type airline name"),
                LineCode = _ioHelper.GetStringFromUser("Type line code")
            };

            var newAirlineId = _airlineService.AddAirline(newAirline);
            Console.WriteLine($"Airline created with ID {newAirlineId}");
            _ioHelper.PrintAndClear("Press any key to continue");
        }

        private void CreateFlightPath()
        {
            Console.WriteLine("Chosen option: 4. Create new flight path\n");
            var planeId = _ioHelper.GetIntFromUser("Type plane Id");
            var airlineId = _ioHelper.GetIntFromUser("Type airline Id");
            var airportId = _ioHelper.GetIntFromUser("Type airport Id");

            var pathCode = _ioHelper.GetStringFromUser("Type 5 numbers path code");
            var result = 0;
            while (!(pathCode.Count() == 5) || !int.TryParse(pathCode, out result))
            {
                pathCode = _ioHelper.GetStringFromUser("Wrong numbers format please try again");
            }

            var flightDay = _ioHelper.GetIntFromUser("Type which day of the week: 1 - 7 (1 = Sunday 7 = Saturday)");
            while (flightDay > 7 || flightDay < 1)
            {
                flightDay = _ioHelper.GetIntFromUser("Wrong numbers format please try again");
            }

            var cost = _ioHelper.GetPriceFromUser("Type ticket price");

            var newFlightPath = new FlightPath
            {
                PathCode = pathCode,
                FlightDay = flightDay,
                FlightTime = _ioHelper.GetDateFromUser("Type flight time", "HH:mm"),
                FlightDurration = _ioHelper.GetDoubleFromUser("Type flight durration"),
                Cost = cost
            };
           
            var newFlightPathId =_flightPathService.AddFlightPath(planeId, airlineId, airportId, newFlightPath);
            Console.WriteLine($"Flight path created with ID {newFlightPathId}");
            _ioHelper.PrintAndClear("Press any key to continue");
        }

        private void CreateUser()
        {
            Console.WriteLine("Chosen option: 5. Create new user\n");

            var newUser = new User
            {
                Login = _ioHelper.GetStringFromUser("Login"),
                PasswordHash = _cryptographyService.GetMd5Hash(_ioHelper.GetStringFromUser("Password")),
                Name = _ioHelper.GetStringFromUser("Type user name"),
                Surname = _ioHelper.GetStringFromUser("Type user surname"),
                BirthDate = _ioHelper.GetDateFromUser("Type user birth date", "dd-MM-yyyy")
            };

            if(_userService.CheckLoginAvalibility(newUser.Login).Result)
            {
                Console.WriteLine("There is already user with that login");
                return;
            }

            var newUserId = _userService.AddUser(newUser).Result;
            Console.WriteLine($"User created with ID {newUserId}");
            _ioHelper.PrintAndClear("Press any key to continue");
        }

        private void PortList()
        {
            Console.WriteLine("Chosen option: 6. Port list\n");

            foreach (var port in _portService.GetAllPorts())
            {
                Console.WriteLine($"#{port.Id}, name : {port.Name}, city: {port.CityName}, country: {port.CountryName}, latitude: {port.Latitude}, longitude: {port.Longitude}");
            }

            _ioHelper.PrintAndClear("Press any key to continue");
        }

        private void PlaneList()
        {
            Console.WriteLine("Chosen option: 7. Plane list\n");

            foreach (var plane in _planeService.GetAllPlanes())
            {
                Console.WriteLine($"#{plane.Id}, manufacturer: {plane.Manifacturer}, model: {plane.Model}, seat count: {plane.SeatCount}");
            }

            _ioHelper.PrintAndClear("Press any key to continue");
        }

        private void AirlineList()
        {
            Console.WriteLine("Chosen option: 8. Airline list\n");

            foreach (var airline in _airlineService.GetAllAirlines())
            {
                Console.WriteLine($"#{airline.Id}, name : {airline.Name}, linecode: {airline.LineCode}");
            }

            _ioHelper.PrintAndClear("Press any key to continue");
        }

        private void FlightPathList()
        {
            Console.WriteLine("Chosen option: 9. Flight path list\n");

            foreach (var flightPath in _flightPathService.GetAllFlightPaths().Result)
            {
                Console.WriteLine(
                    $"#{flightPath.Id}, Airplane: {flightPath.PlaneManifacturer} {flightPath.PlaneModel}," +
                    $" seat count: {flightPath.PlaneSeatCount}," +
                    $" Airline: {flightPath.AirlineName}, Flight code: {flightPath.FlightCode}," +
                    $" Destination: {flightPath.PortName} in {flightPath.PortCityName} {flightPath.PortCountryName}," +
                    $" Cost: {flightPath.Cost}, Flight day: {_ioHelper.GetDayOfTheWeek(flightPath.FlightDay)}," +
                    $" Flight time: {flightPath.FlightTime.ToString("HH:mm")}");
            }

            _ioHelper.PrintAndClear("Press any key to continue");
        }

        private void UserList()
        {
            Console.WriteLine("Chosen option: 10. User list\n");

            foreach (var user in _userService.GetAllUsers())
            {
                var date = user.BirthDate.HasValue? user.BirthDate.Value.ToString("d") : "[empty]";
                Console.WriteLine($"#{user.Id}, name: {user.Name}, surname: {user.Surname}, birth date: {date}");
            }

            _ioHelper.PrintAndClear("Press any key to continue");
        }

        private void EditUser()
        {
            Console.WriteLine("Chosen option: 11. Edit user\n");

            var chosenUser = _userService.GetUserById(_ioHelper.GetIntFromUser("Type user id"));

            var newName = _ioHelper.GetStringFromUser($"Type new name, current name {chosenUser.Name}");
            if (newName == "")
            {
                newName = chosenUser.Name;
            }

            var newSurname = _ioHelper.GetStringFromUser($"Type new surname, current surname {chosenUser.Surname}");
            if (newSurname == "")
            {
                newSurname = chosenUser.Surname;
            }
            var choosenUserBirthDate = chosenUser.BirthDate.HasValue ? chosenUser.BirthDate.Value.ToString("d") : "[empty]";
            var newBirthDate = _ioHelper.GetDateFromUser($"Type user birth date, current birth date {choosenUserBirthDate}", "dd-MM-yyyy");
            if (newBirthDate == null)
            {
                newBirthDate = chosenUser.BirthDate;
            }

            chosenUser.Name = newName;
            chosenUser.Surname = newSurname;
            chosenUser.BirthDate = newBirthDate;

            _userService.UpdateUser(chosenUser);
            _ioHelper.PrintAndClear("Press any key to continue");
        }

        public void SellTicket()
        {
            Console.WriteLine("Chosen option: 12. Sell ticket\n");
            var userId = _ioHelper.GetIntFromUser("Type user Id");
            var flightId = _ioHelper.GetIntFromUser("Type flight Id");
            var flightDate = _ioHelper.GetDateFromUser("Type flight date", "dd-MM-yyyy");
            while(!flightDate.HasValue)
            {
                Console.WriteLine("no date typed, please try again");
                flightDate = _ioHelper.GetDateFromUser("Type flight date", "dd-MM-yyyy");
            }

            var flightInfo = _flightPathService.GetFlightPathById(flightId).Result;
            var flightDay = _ioHelper.GetDayOfTheWeek(flightInfo.FlightDay);

            while (flightDay != flightDate.Value.DayOfWeek.ToString())
            {
                Console.WriteLine($"This flight is only avaliable on {flightDay} you typed date with day of the week: {flightDate.Value.DayOfWeek.ToString()} press any key to continue or escape to return to menu");
                
                if (Console.ReadKey().Key == ConsoleKey.Escape)
                {
                    Console.Clear();
                    return;
                }
                else
                {
                    flightDate = _ioHelper.GetDateFromUser("Type flight date", "dd-MM-yyyy");
                } 
            }

            var newTicket = new Ticket 
            { 
                Active = true, 
                FlightDate = flightDate
            };

            var newTicketId = _ticketService.AddTicket(userId, flightId, newTicket).Result;
            Console.WriteLine($"New transaction created with id: {newTicketId}");
            _ioHelper.PrintAndClear("Press any key to continue");
        }

        public void CancelTicket()
        {
            Console.WriteLine("Chosen option: 13. Cancel ticket\n");

            if (_ticketService.GetAllNotCanceledTickets().Result.Count == 0)
            {
                _ioHelper.PrintAndClear("There are no ticket to cancel, press any key to continue");
                return;
            }

            Console.WriteLine("Avaliable transactions:");
            foreach(var ticket in _ticketService.GetAllNotCanceledTickets().Result)
            {
                Console.WriteLine($"{ticket.Id}, {ticket.FlightCode}, user name : {ticket.User.Name} {ticket.User.Surname}");
            }

            var chosenTicket = _ticketService.GetTicketById(_ioHelper.GetIntFromUser("Type transaction id"));
            if (!chosenTicket.Active)
            {
                Console.WriteLine("Transaction with this ID is no longer active");
                _ioHelper.PrintAndClear("Press any key to continue");
                return;
            }

            var chosenTicketDistance = _distanceCalculator.CalculateDistance(AirportData.Latitude, AirportData.Longitude, chosenTicket.FlightPath.Port.Latitude, chosenTicket.FlightPath.Port.Longitude);
            chosenTicket.User.DistanceTraveled -= chosenTicketDistance;
            chosenTicket.Active = false;
            _ticketService.UpdateTicket(chosenTicket);
            _ioHelper.PrintAndClear("Press any key to continue");
        }

        public void TicketHistory()
        {
            Console.WriteLine("Chosen option: 14. Ticket history\n");

            var userId = _ioHelper.GetIntFromUser("Type user Id");
            var allTickets = _ticketService.GetUserAllTickets(userId);
            var notCanceledTickets = _ticketService.GetUserNotCanceledTickets(userId);
            var canceledTickets = _ticketService.GetUserCanceledTickets(userId);
            double allTicketsCost = 0;
            double notCanceledCost = 0;
            double CanceledCost = 0;

            Console.WriteLine("Not canceled tickets");
            foreach(var ticket in notCanceledTickets.Result)
            {
                string time = _ioHelper.CheckForAValidDateTime(ticket.FlightDate);
                
                Console.WriteLine($"{ticket.FlightCode}, {time}, {ticket.AirportName}, {ticket.Cost}");
            }

            Console.WriteLine("____________________________________________");
            Console.WriteLine("Canceled tickets");
            foreach(var ticket in canceledTickets.Result)
            {
                string time = _ioHelper.CheckForAValidDateTime(ticket.FlightDate);
                Console.WriteLine($"{ticket.FlightCode}, {time}, {ticket.AirportName}, {ticket.Cost}");
            }

            Console.WriteLine("____________________________________________");
            foreach (var ticket in allTickets.Result)
            {
                allTicketsCost += ticket.Cost;
            }

            Console.WriteLine($"All tickets cost: {allTicketsCost}");
            Console.WriteLine("____________________________________________");
            foreach(var ticket in notCanceledTickets.Result)
            {
                notCanceledCost += ticket.Cost;               
            }

            Console.WriteLine($"Not canceled tickets cost: {notCanceledCost}");
            Console.WriteLine("____________________________________________");
            foreach (var ticket in canceledTickets.Result)
            {
                CanceledCost += ticket.Cost;
            }

            Console.WriteLine($"Canceled tickets cost: {CanceledCost}");
            Console.WriteLine("____________________________________________");

            if (_ioHelper.GetStringFromUser("type PRINT to print all reports to json file type") == "PRINT")
            {
                var path = _ioHelper.GetStringFromUser("type path to files:");
                _jsonConversionService.Save($"{path}/all.json", allTickets.Result);
                _jsonConversionService.Save($"{path}/notCanceled.json", notCanceledTickets.Result);
                _jsonConversionService.Save($"{path}/canceled.json", canceledTickets.Result);
            }

            _ioHelper.PrintAndClear("Press any key to continue");
        }

        public void FlightBoard()
        {
            Console.WriteLine("Chosen option: 15. Flight Board\n");
            var input = false;

            while(!input)
            {
                _flightBoardDisplay.Start();

                if (Console.ReadKey().Key == ConsoleKey.Enter)
                {
                    _flightBoardDisplay.Stop();
                    input = true;                    
                }
            }

            Console.Clear();
        }

        public void DisplayArivalBoardData()
        {
            _flightBoardDisplay.DisplayArrivalFlightBoard();
        }

        private void Exit()
        {;
            _eventStoreConnectionService.CloseConnection();
            Environment.Exit(0);
        }
    }
}
