import { Component, OnInit } from '@angular/core';
import { FlightBoardService } from '../services/flight-board/flight.board.service';
import { FlightInfo } from '../models/flight-info';
import { Router } from '@angular/router';

@Component({
  selector: 'app-departure-board',
  templateUrl: './departure-board.component.html',
  styleUrls: ['./departure-board.component.scss']
})
export class DepartureBoardComponent implements OnInit {

  flights: FlightInfo[] = [];
  constructor(private router: Router,
    private flightBoardService : FlightBoardService) { }

  ngOnInit() {
    this.flightBoardService.getDepartures()
    .subscribe(
      (items) => {
        this.flights = items
      },
      () => {
      }
    );
  }


  refresh() : void {
    this.flightBoardService.getDepartures()
    .subscribe(
      (items) => {
        this.flights = items
      },
      () => {
      }
    );
  }

  goBack() : void {
    this.router.navigate([''])
  }
}
