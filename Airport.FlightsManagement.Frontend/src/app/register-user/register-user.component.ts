import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UserService } from '../services/user-service/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Md5 } from 'ts-md5';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {

  constructor(
    private userService : UserService,
    private toastr: ToastrService,
    private router: Router,
  ) { }
  newUser : User;
  maxDate = new Date();
  notAvalible : boolean;
  ngOnInit() {
    this.newUser = new User();
    
  }


  addUser() : void{

    if(!this.newUser.login || !this.newUser.passwordHash || !this.newUser.name || !this.newUser.surname || !this.newUser.birthDate || this.newUser.birthDate > this.maxDate)
    {
      return;
    }

    this.newUser.passwordHash = Md5.hashStr(this.newUser.passwordHash).toString()
    this.userService.avalibleLogin(this.newUser.login)
    .subscribe(
      (avalibility) => {this.notAvalible = avalibility;
        if(this.notAvalible == false)
        {
        this.userService.add(this.newUser)
        .subscribe(
          (userId) => {
            this.toastr.success('User registered succesfully id =  ' + userId, 'Registration')
            this.router.navigate(['/login'])
          },
          () => {
            this.toastr.error('An error occured while user registration', 'Registration')
          }
        );
        }
        else{
          this.toastr.error('An error occured while user registration there is alredy user with this login', 'Registration')
        } },
      (err) => {this.toastr.error(err)}
    );
    
  }   
  
  goBack() : void {
    this.router.navigate([''])
  }
}
