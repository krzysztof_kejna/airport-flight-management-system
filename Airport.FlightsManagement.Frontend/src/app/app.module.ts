import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { 
  MatDatepickerModule, 
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FlightListComponent } from './flight-list/flight-list.component';
import { IsUserLoggedGuard } from './security/is-user-logged.guard';
import { LogInComponent } from './log-in/log-in.component';
import { StorageServiceModule } from 'angular-webstorage-service';
import { LocalStorageService } from './services/local-storage/local-storage.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { BuyTicketComponent } from './buy-ticket/buy-ticket.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { ArrivalBoardComponent } from './arrival-board/arrival-board.component';
import { DepartureBoardComponent } from './departure-board/departure-board.component';
import { TicketHistoryComponent } from './ticket-history/ticket-history.component';
import { HomeComponent } from './home/home.component';
import { LogOutComponent } from './log-out/log-out.component';

@NgModule({
  declarations: [
    AppComponent,
    FlightListComponent,
    LogInComponent,
    BuyTicketComponent,
    RegisterUserComponent,
    ArrivalBoardComponent,
    DepartureBoardComponent,
    TicketHistoryComponent,
    HomeComponent,
    LogOutComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    StorageServiceModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule 
  ],
  providers: [    
    IsUserLoggedGuard,
    LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
