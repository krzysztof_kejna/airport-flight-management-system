import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from '../services/local-storage/local-storage.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import {Md5} from 'ts-md5/dist/md5';
import { User } from '../models/user';
import { UserAuthData } from '../models/user-auth-data';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
userName : string = '';
userPassword : string = '';
loggedUser : User = new User();

  constructor(
    private http : HttpClient,
    private toastr : ToastrService,
    private router: Router,
    private localStorageService: LocalStorageService,
    private appComponent : AppComponent,
    ) { }

  ngOnInit() {
  }

logIn() : void{

  if(!this.userName || !this.userPassword)
  {
    return;
  }

  let userAuthData : UserAuthData = {
    userName : this.userName,
    password : Md5.hashStr(this.userPassword).toString()
  }
     this.http.post<boolean>('http://localhost:10500/api/users/auth', userAuthData)
     .subscribe(
       (result) => {this.onAuthenticated(result)},
       () => {this.onAuthenticated(false)}
     );
  }

  onAuthenticated(isAuth : boolean) : void {
    this.localStorageService.save(isAuth)
    if(isAuth){
      this.toastr.success('User authenticated succesfully', 'Success');
      this.http.get<User>('http://localhost:10500/api/users/' + this.userName)
      .subscribe(
        (result) => {this.localStorageService.saveUser(result);
          this.appComponent.update();
         },
        () => {}
      );
      this.router.navigate(['']);
    }
    else{
      this.toastr.error("User loggon failed", 'Error')
    }
  }

  goBack() : void {
    this.router.navigate([''])
  }
}