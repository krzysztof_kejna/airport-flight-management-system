import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FlightsService } from '../services/flight-service/flights.service';
import { FlightItem } from '../models/flight-item';
import { Ticket } from '../models/ticket';
import { TicketService } from '../services/ticket-service/ticket.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from '../services/local-storage/local-storage.service';
import { User } from '../models/user';

@Component({
  selector: 'app-buy-ticket',
  templateUrl: './buy-ticket.component.html',
  styleUrls: ['./buy-ticket.component.scss']
})
export class BuyTicketComponent implements OnInit {

  date : any;
  startDate = new Date();
  selectedFlight : FlightItem;
  newTicket : Ticket = new Ticket();
  selectedUser : User;
  constructor(private route : ActivatedRoute,
    private flightService : FlightsService,
    private ticketService : TicketService,
    private toastr: ToastrService,
    private router: Router,
    private localStorageService : LocalStorageService) { }

  ngOnInit() {
    const flightId = Number(this.route.snapshot.paramMap.get('id'));
    this.flightService.get(flightId)
    .subscribe((item) => {
      this.selectedFlight = item;
    },
    () => {
      console.log('error when selecting flight')
    });
    this.newTicket.flightPathId = flightId;
    this.selectedUser = this.localStorageService.readUser();
    this.newTicket.userId = this.selectedUser.id;
    this.newTicket.active = true;   
  }

  dateFilter: (date: Date | null) => boolean =
    (date: Date | null) => {
    const day = date.getDay();
    return day === (this.selectedFlight.flightDay - 1);
    }
  
  buyTicket() : void {
      if(this.newTicket.flightDate == null ||(this.selectedFlight.flightDay - 1) != this.newTicket.flightDate.getDay())
      {
        return
      }
    
    this.ticketService.add(this.newTicket)
    .subscribe(
      () => {
        this.toastr.success('Ticket bought succesfully succesfully', 'buying ticket')
        this.router.navigate(['tickets/bought'])
      },
      () => {
        this.toastr.error('An error occured when buying', 'buying ticket')
      }
    );   
  }

  goBack() : void {
    this.router.navigate(['flights'])
  }
}
