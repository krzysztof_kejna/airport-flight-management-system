export class User{
    id : number
    login : string
    passwordHash : string
    name : string
    surname : string
    birthDate : Date
    distanceTraveled : number
}