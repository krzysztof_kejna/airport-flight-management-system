import { User } from './user';
import { FlightItem } from './flight-item';

export class Ticket{
    id : number
    flightPathId : number
    flightPath : FlightItem
    userId : number
    user : User
    flightDate : Date
    active : Boolean
    cost : number
}