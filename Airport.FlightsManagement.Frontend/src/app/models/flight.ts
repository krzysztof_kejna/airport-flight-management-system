export class Flight{
    id : number
    planeManifacturer : string
    planeModel: string
    planeSeatCount;
    airlineName: string
    portName: string
    portCityName: string
    portCountryName: string
    cost : number
    flightDay : number
    flightTime : Date
    flightDurration : number
}