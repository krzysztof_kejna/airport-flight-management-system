export class FlightInfo{
    departureAirportName : string
    arrivalAirportName : string
    departureDate : Date
    departureStatus : string
    arrivalDate : Date
    arrivalStatus : string
    flightCode : string
}