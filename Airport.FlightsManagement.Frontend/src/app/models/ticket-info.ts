import { User } from './user';

export class TicketInfo{
    id : number
    flightCode : string
    flightDate : Date
    airportName : string
    cost : number
    user : User
}