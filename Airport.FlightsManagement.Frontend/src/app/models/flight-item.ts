export class FlightItem{
    id : number
    planeId : number
    plane : {
        manifacturer : string,
        model : string,
        seatCount : number
    }
    airlineId : number
    airline : {name: string,
    lineCode: string}
    portId : number
    port : {name : string,
    cityname : string,
    countryName : string,
    latitude : number,
    longitude : number}
    pathCode : string
    flightDay : number
    flightTime : Date
    flightDurration : number
    cost : number
}