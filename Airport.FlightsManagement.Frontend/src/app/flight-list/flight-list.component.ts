import { Component, OnInit } from '@angular/core';
import { Flight } from '../models/flight';
import { Router } from '@angular/router';
import { FlightsService } from '../services/flight-service/flights.service';

enum Weekdays {
  Monday = 2,
  Tuesday = 3,
  Wednesday = 4,
  Thursday = 5,
  Friday = 6,
  Saturday = 7,
  Sunday = 1
}

@Component({
  selector: 'app-flight-list',
  templateUrl: './flight-list.component.html',
  styleUrls: ['./flight-list.component.scss']
})
export class FlightListComponent implements OnInit {

  flights: Flight[] = [];
  day : string;
  constructor(private router: Router,
    private flightsService : FlightsService) { }
  ngOnInit() {
    
    this.flightsService.getFlights()
    .subscribe(
      (items) => {
        this.flights = items
      },
      () => {
      }
    );
  }

  getDayOfTheWeek(dayOfTheWeek : number) : string
  {
    this.day = Weekdays[dayOfTheWeek];
    return this.day;
  }

  buyTicket(flightId : number) : void {
    this.router.navigate(['/tickets/' + flightId])
  }

  goBack() : void {
    this.router.navigate([''])
  }
}
