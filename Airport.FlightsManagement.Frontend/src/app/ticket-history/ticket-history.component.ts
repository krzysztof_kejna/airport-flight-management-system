import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { LocalStorageService } from '../services/local-storage/local-storage.service';
import { TicketInfo } from '../models/ticket-info';
import { TicketService } from '../services/ticket-service/ticket.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ticket-history',
  templateUrl: './ticket-history.component.html',
  styleUrls: ['./ticket-history.component.scss']
})
export class TicketHistoryComponent implements OnInit {

  constructor(private localStorageService : LocalStorageService,
    private ticketService : TicketService,
    private router : Router) { }
  tickets: TicketInfo[] = [];
  selectedUser : User = this.localStorageService.readUser();
  ngOnInit() {
    this.ticketService.get(this.selectedUser.id)
    .subscribe(
      (items) => {
        this.tickets = items
      },
      () => {
      }
    );
  }

  goBack() : void {
    this.router.navigate([''])
  }
}
