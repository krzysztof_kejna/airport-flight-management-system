import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Flight } from './models/flight';
import { FlightListComponent } from './flight-list/flight-list.component';
import { BuyTicketComponent } from './buy-ticket/buy-ticket.component';
import { IsUserLoggedGuard } from './security/is-user-logged.guard';
import { LogInComponent } from './log-in/log-in.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { ArrivalBoardComponent } from './arrival-board/arrival-board.component';
import { DepartureBoardComponent } from './departure-board/departure-board.component';
import { TicketHistoryComponent } from './ticket-history/ticket-history.component';
import { HomeComponent } from './home/home.component';
import { LogOutComponent } from './log-out/log-out.component';


const routes: Routes = [
  {path: '', redirectTo : 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent, pathMatch: 'full'},
  {path: 'flights', component: FlightListComponent, pathMatch: 'full'},
  {path: 'login', component: LogInComponent, pathMatch: 'full'},
  {path: 'logout', component: LogOutComponent, pathMatch: 'full'},
  {path: 'registration', component: RegisterUserComponent, pathMatch: 'full'},
  {path: 'tickets', children:[
    {path: 'bought', component: TicketHistoryComponent, pathMatch: 'full', canActivate: [IsUserLoggedGuard]},
    {path: ':id', component: BuyTicketComponent, pathMatch: 'full', canActivate: [IsUserLoggedGuard]}
  ]},
  {path: 'arrivals', component: ArrivalBoardComponent, pathMatch: 'full'},
  {path: 'departures', component: DepartureBoardComponent, pathMatch: 'full'},
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
