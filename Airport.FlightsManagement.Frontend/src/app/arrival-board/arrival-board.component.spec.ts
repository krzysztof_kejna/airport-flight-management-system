import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrivalBoardComponent } from './arrival-board.component';

describe('ArrivalBoardComponent', () => {
  let component: ArrivalBoardComponent;
  let fixture: ComponentFixture<ArrivalBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArrivalBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrivalBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
