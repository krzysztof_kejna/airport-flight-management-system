import { Component, OnInit } from '@angular/core';
import { FlightBoardService } from '../services/flight-board/flight.board.service';
import { FlightInfo } from '../models/flight-info';
import { Router } from '@angular/router';

@Component({
  selector: 'app-arrival-board',
  templateUrl: './arrival-board.component.html',
  styleUrls: ['./arrival-board.component.scss']
})
export class ArrivalBoardComponent implements OnInit {

  flights: FlightInfo[] = [];
  constructor(private router: Router,
    private flightBoardService : FlightBoardService) { }

  ngOnInit() {
    this.flightBoardService.getArrivals()
    .subscribe(
      (items) => {
        this.flights = items
      },
      () => {
      }
    );
  }


  refresh() : void {
    this.flightBoardService.getArrivals()
    .subscribe(
      (items) => {
        this.flights = items
      },
      () => {
      }
    );
  }

  goBack() : void {
    this.router.navigate([''])
  }
}
