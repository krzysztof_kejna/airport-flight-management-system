import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from '../services/local-storage/local-storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private router : Router,
    private localStorageService : LocalStorageService) { }
    logged : boolean;
  ngOnInit() {
    let isAuthenticated : boolean = this.localStorageService.read();

    if(!isAuthenticated)
    {
      this.logged = false;
    }
    else{
      this.logged = true;
    }
  }

  flights() : void {
    this.router.navigate(['flights'])
  }

  arrivals() : void {
    this.router.navigate(['arrivals'])
  }

  departures() : void {
    this.router.navigate(['departures'])
  }

  tickets() : void {
    this.router.navigate(['tickets/bought'])
  }

  logIn() : void {
    this.router.navigate(['login'])
  }

  register() : void {
    this.router.navigate(['registration'])
  }
}
