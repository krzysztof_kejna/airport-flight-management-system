import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../services/local-storage/local-storage.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppComponent } from '../app.component';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-log-out',
  templateUrl: './log-out.component.html',
  styleUrls: ['./log-out.component.scss']
})
export class LogOutComponent implements OnInit {

  constructor(private localStorageService : LocalStorageService,
     private router : Router,
     private appComponent : AppComponent,
     private http : HttpClient) { }

  ngOnInit() {
    this.http.get<boolean>('http://localhost:10500/api/users/logout')
    .subscribe(
      (result) => {this.localStorageService.save(result);
        this.localStorageService.saveUser(null);
        this.appComponent.update();
       },
      () => {}
    );
  }

  goBack() : void {
    this.router.navigate([''])
  }
}
