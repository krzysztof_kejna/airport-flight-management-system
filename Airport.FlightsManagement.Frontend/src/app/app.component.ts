import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from './services/local-storage/local-storage.service';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  title = 'airport-flight-management-system-frontend';
  
  constructor(private router : Router,
    private localStorageService : LocalStorageService) { }

   logged : boolean = false;
   user : User = null
  ngOnInit() {
    this.logged = this.localStorageService.read();
    this.user = this.localStorageService.readUser();
  }

  update()
  {
    this.logged = this.localStorageService.read();
    this.user = this.localStorageService.readUser();
  }
}


