import { Injectable } from '@angular/core';
import { FlightInfo } from 'src/app/models/flight-info';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FlightBoardService {

  constructor(private http : HttpClient) { }

  public getArrivals() : Observable<FlightInfo[]> {
    return this.http.get<FlightInfo[]>('http://localhost:10500/api/board/arrived');
  }

  public getDepartures() : Observable<FlightInfo[]> {
    return this.http.get<FlightInfo[]>('http://localhost:10500/api/board/departured');
  }
}
