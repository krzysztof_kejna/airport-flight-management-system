import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Flight } from 'src/app/models/flight';
import { FlightItem } from 'src/app/models/flight-item';

@Injectable({
  providedIn: 'root'
})
export class FlightsService {

  constructor(private http: HttpClient) { }

  public getFlights() : Observable<Flight[]> {
    return this.http.get<Flight[]>('http://localhost:10500/api/flights');
  }

  public get(flightId : number) : Observable<FlightItem> {
    return this.http.get<FlightItem>('http://localhost:10500/api/flights/' + flightId);
  }
}