import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Ticket } from 'src/app/models/ticket';
import { TicketInfo } from 'src/app/models/ticket-info';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private http: HttpClient) { }

  public add(ticket : Ticket) : Observable<number> {
      return this.http.post<number>('http://localhost:10500/api/tickets', ticket);
  }

  public get(userId : number) : Observable<TicketInfo[]>{
    return this.http.get<TicketInfo[]>('http://localhost:10500/api/tickets/bought/' + userId);
  }

}