import { Inject, Injectable  } from '@angular/core';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { User } from 'src/app/models/user';


@Injectable()
export class LocalStorageService {
  private isAuthKey = 'airport_kk_isauth';
  private LogedUser = 'LogedUser';
  constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService)
  {
  }
  saveUser(user : User): void{
    this.storage.set(this.LogedUser, user);
  }
  save(val : boolean): void {
    this.storage.set(this.isAuthKey, val);
  }
  read(): boolean {
    var isAuth: boolean = this.storage.get(this.isAuthKey);
    return isAuth;
  }
  readUser(): User {
    var user: User = this.storage.get(this.LogedUser);
    return user;
  }
}
