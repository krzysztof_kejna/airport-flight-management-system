import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

    public getUser(login : string) : Observable<User> {
        return this.http.get<User>('http://localhost:10500/api/users/' + login);
    }

    public add(user : User) : Observable<number> {
        return this.http.post<number>('http://localhost:10500/api/users', user);
    }

    public avalibleLogin(login : string) : Observable<boolean>{
        return this.http.get<boolean>('http://localhost:10500/api/users/avalible/' + login)
    }

    public 
}