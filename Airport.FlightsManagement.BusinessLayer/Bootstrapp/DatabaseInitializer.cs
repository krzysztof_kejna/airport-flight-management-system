﻿using Airport.FlightsManagement.DataLayer;
using System;

namespace Airport.FlightsManagement.BusinessLayer.Bootstrapp
{
    public interface IDatabaseInitializer
    {
        void Initialize();
    }

    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly Func<IAirportFlightsManagementDbContext> _dbContextFactory;

        public DatabaseInitializer(Func<IAirportFlightsManagementDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public void Initialize()
        {
            using (var ctx = _dbContextFactory())
            {
                ctx.Database.EnsureCreated();
            }
        }
    }
}
