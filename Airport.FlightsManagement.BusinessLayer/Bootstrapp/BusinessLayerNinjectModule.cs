﻿using Airport.FlightsManagement.BusinessLayer.Services;
using Ninject.Modules;

namespace Airport.FlightsManagement.BusinessLayer.Bootstrapp
{
    public class BusinessLayerNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IPortService>().To<PortService>();
            Kernel.Bind<IPlaneService>().To<PlaneService>();
            Kernel.Bind<IAirlineService>().To<AirlineService>();
            Kernel.Bind<IFlightPathService>().To<FlightPathService>();
            Kernel.Bind<IUserService>().To<UserService>();
            Kernel.Bind<IGeoLocationService>().To<GeoLocationService>();
            Kernel.Bind<ITicketCostCalculationService>().To<TicketCostCalculationService>();
            Kernel.Bind<ITicketService>().To<TicketService>();
            Kernel.Bind<IJsonConversionService>().To<JsonConversionService>();
            Kernel.Bind<IDistanceCalculator>().To<DistanceCalculator>();
            Kernel.Bind<IFlightBoardService>().To<FlightBoardService>();
            Kernel.Bind<ISystemTimeService>().To<SystemTimeService>();
            Kernel.Bind<ICryptographyService>().To<CryptographyService>();
            Kernel.Bind<IEventStoreConnectionService>().To<EventStoreConnectionService>();

            Kernel.Bind<IDatabaseInitializer>().To<DatabaseInitializer>();
        }
    }
}
