﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.BusinessLayer.Models
{
    public class Flight
    {
        public string DepartureAirportName { get; set; }
        public string ArrivalAirportName { get; set; }
        public DateTime? DepartureDate { get; set; }
        public string DepartureStatus { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string ArrivalStatus { get; set; }
        public string FlightCode { get; set; }
    }
}