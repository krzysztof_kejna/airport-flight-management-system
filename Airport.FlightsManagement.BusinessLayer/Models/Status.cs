﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.BusinessLayer.Models
{
    public class Status
    {
        public enum DepartureStatus
        {
            awaiting = 0,
            checkIn = 1,
            checkInClosedGoToGate = 2,
            boarding = 3,
            departed = 4,
            error = 9
        }

        public enum ArrivalStatus
        {
            landing = 0,
            landed = 1,
            collectLuggage = 2,
            flightProcesses = 3,
            error = 9
        }
    }
}
