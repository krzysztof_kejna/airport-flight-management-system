﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.BusinessLayer.Models
{
    public class CreatedPathFlights
    {
        public int Id;
        public string PlaneManifacturer;
        public string PlaneModel;
        public int PlaneSeatCount;
        public string AirlineName;
        public string FlightCode;
        public string PortName;
        public string PortCityName;
        public string PortCountryName;
        public double Cost;
        public int FlightDay;
        public DateTime FlightTime;
        public double FlightDurration;
    }
}