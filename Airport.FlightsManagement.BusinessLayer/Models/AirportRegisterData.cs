﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.BusinessLayer.Models
{
    class AirportRegisterData
    {
        public AirportRegisterData()
        {
        }

        public AirportRegisterData(AirportRegisterData airportRegisterData)
        {
            AirportName = airportRegisterData.AirportName;
            ArrivalsStreamName = airportRegisterData.ArrivalsStreamName;
        }

        public string AirportName { get; set; }
        public string ArrivalsStreamName { get; set; }
    }
}
