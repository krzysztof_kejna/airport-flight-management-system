﻿using Airport.FlightsManagement.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.BusinessLayer.Models
{
    public class CreatedTickets
    {
        public int Id;
        public string FlightCode;
        public DateTime? FlightDate;
        public string AirportName;
        public double Cost;
        public User User;
    }
}
