﻿using GeoCoordinatePortable;

namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface IGeoLocationService
    {
        GeoCoordinate GetCoordinations(double latitude, double longitude);
    }

    public class GeoLocationService : IGeoLocationService
    {
        public GeoCoordinate GetCoordinations(double latitude, double longitude)
        {
                    var coordinations = new GeoCoordinate(latitude, longitude);
                    return coordinations;
        }
    }
}
