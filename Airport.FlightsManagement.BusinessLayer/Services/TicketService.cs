﻿using Airport.FlightsManagement.BusinessLayer.Models;
using Airport.FlightsManagement.DataLayer;
using Airport.FlightsManagement.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface ITicketService
    {
        Task<int> AddTicket(int userId, int flightPathId, Ticket ticket);
        Task<List<CreatedTickets>> GetUserAllTickets(int userId);
        Task<List<CreatedTickets>> GetUserCanceledTickets(int userId);
        Task<List<CreatedTickets>> GetUserNotCanceledTickets(int userId);
        Task<List<CreatedTickets>> GetAllNotCanceledTickets();
        void UpdateTicket(Ticket ticket);
        Ticket GetTicketById(int id);
    }

    public class TicketService : ITicketService
    {
        private readonly Func<IAirportFlightsManagementDbContext> _dbContextFactory;
        private readonly IUserService _userService;
        private readonly IFlightPathService _flightPathService;
        private readonly ITicketCostCalculationService _ticketCostCalculationService;
        private readonly IDistanceCalculator _distanceCalculator;

        public TicketService(IUserService userService,
            IFlightPathService flightPathService,
            Func<IAirportFlightsManagementDbContext> dbContextFactory,
            ITicketCostCalculationService ticketCostCalculationService,
            IDistanceCalculator distanceCalculator)
        {
            _userService = userService;
            _flightPathService = flightPathService;
            _dbContextFactory = dbContextFactory;
            _ticketCostCalculationService = ticketCostCalculationService;
            _distanceCalculator = distanceCalculator;
        }

        public async Task<int> AddTicket(int userId, int flightPathId, Ticket ticket)
        {
            var airportLatitude = 54.22;
            var airportLongitude = 18.38;
            var choosenUser = _userService.GetUserById(userId);
            var choosenFlightPath = _flightPathService.GetFlightPathById(flightPathId).Result;

            if (choosenUser == null || choosenFlightPath == null)
            {
                throw new Exception("There is no one or more item with typed id");
            }
            var cost = _ticketCostCalculationService.TicketCostCalculation(userId, choosenFlightPath.Cost);
            var userDistanceTraveled = _distanceCalculator.CalculateDistance(airportLatitude, airportLongitude, choosenFlightPath.Port.Latitude, choosenFlightPath.Port.Longitude);
            var newUserInfo = _userService.GetUserById(userId);
            newUserInfo.DistanceTraveled += userDistanceTraveled;
            _userService.UpdateUser(newUserInfo);

            ticket.Cost = cost;
            ticket.UserId = choosenUser.Id;
            ticket.FlightPathId = choosenFlightPath.Id;

            using (var context = _dbContextFactory())
            {
                var newTicketEntinty = context.Tickets.Add(ticket);
                await context.SaveChangesAsync();
                return newTicketEntinty.Entity.Id;
            }
        }

        public Ticket GetTicketById(int id)
        {
            using (var context = _dbContextFactory())
            {
                return context.Tickets
                    .Include(p => p.User)
                    .Include(p => p.FlightPath.Port)
                    .FirstOrDefault(p => p.Id == id);
            }
        }

        public void UpdateTicket(Ticket ticket)
        {
            using (var context = _dbContextFactory())
            {
                context.Tickets.Update(ticket);
                context.SaveChanges();
            }
        }

        public async Task<List<CreatedTickets>> GetUserAllTickets(int userId)
        {
            using (var context = _dbContextFactory())
            {
                var ticketsSubQuery = context.Tickets
                    .Where(ticket => ticket.User.Id == userId)
                    .OrderByDescending(ticket => ticket.FlightDate);
                return await GetValidCreatedPathFlightsItems(ticketsSubQuery);
            }
        }

        public async Task<List<CreatedTickets>> GetUserNotCanceledTickets(int userId)
        {
            using (var context = _dbContextFactory())
            {
                var ticketsSubQuery = context.Tickets
                    .Where(ticket => ticket.User.Id == userId)
                    .Where(ticket => ticket.Active == true)
                    .OrderByDescending(ticket => ticket.FlightDate);
                return await GetValidCreatedPathFlightsItems(ticketsSubQuery);
            }
        }

        public async Task<List<CreatedTickets>> GetUserCanceledTickets(int userId)
        {
            using (var context = _dbContextFactory())
            {
                var ticketsSubQuery = context.Tickets
                    .Where(ticket => ticket.User.Id == userId)
                    .Where(ticket => ticket.Active == false)
                    .OrderByDescending(ticket => ticket.FlightDate);
                return await GetValidCreatedPathFlightsItems(ticketsSubQuery);
            }
        }

        public async Task<List<CreatedTickets>> GetAllNotCanceledTickets()
        {
            using (var context = _dbContextFactory())
            {
                var ticketsSubQuery = context.Tickets
                    .Where(ticket => ticket.Active == true)
                    .OrderBy(ticket => ticket.Id);
                return await GetValidCreatedPathFlightsItems(ticketsSubQuery);
            }
        }

        private async Task<List<CreatedTickets>> GetValidCreatedPathFlightsItems(IQueryable<Ticket> ticketsQueryBase)
        {
            var tickets = await ticketsQueryBase
                .Include(t => t.FlightPath)
                .Include(t => t.User)
                .Select(tickets => new CreatedTickets
                {
                    Id = tickets.Id,
                    FlightCode = tickets.FlightPath.Airline.LineCode + tickets.FlightPath.PathCode,
                    FlightDate = tickets.FlightDate.Value.AddHours(tickets.FlightPath.FlightTime.Value.Hour).AddMinutes(tickets.FlightPath.FlightTime.Value.Minute),
                    AirportName = tickets.FlightPath.Port.Name,
                    Cost = tickets.Cost,
                    User = tickets.User
                })
                .ToListAsync();

            return tickets;
        }
    }
}
