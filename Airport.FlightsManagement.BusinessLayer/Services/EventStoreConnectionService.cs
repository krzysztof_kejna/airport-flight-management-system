﻿using Airport.FlightsManagement.BusinessLayer.Models;
using EventStore.ClientAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface IEventStoreConnectionService
    {
        void CloseConnection();
        void Connect();
        void Register();
        void SendDepartureData(List<Flight> x);
        Task SendFlightBoardEventStore();
        void SubscribeForArrivals();
    }

    public class EventStoreConnectionService : IEventStoreConnectionService
    {
        private IEventStoreConnection _connection = null;
        private const string stream = "flightmanagement-departures-stream";
        private const string ArrivalsStreamName = "flightmanagement-arrivals-krzysiek-stream";
        const string RegistrationStreamName = "flightmanagement-airport-registration-stream";
        private readonly IFlightBoardService _flightBoardService;
        public static List<Flight> lastFlights = new List<Flight>();

        public EventStoreConnectionService(IFlightBoardService flightBoardService)
        {
            _flightBoardService = flightBoardService;
        }

        public async Task SendFlightBoardEventStore()
        {
            string currentdata = "";
            var task = new Task(() =>
            {
                while (_connection != null)
                {
                    var data = "";
                    var x = _flightBoardService.GetFlightData().Result;

                    foreach (var flight in x)
                    {
                        data = string.Join("", flight.DepartureStatus, data);
                    }

                    while (data != currentdata)
                    {
                        SendDepartureData(x);
                        currentdata = data;
                    }
                }
            });

            task.Start();
        }

        private EventData GetRegistrationEvent()
        {
            var registrationData = new AirportRegisterData
            {
                AirportName = "KrzysiekAirport",
                ArrivalsStreamName = ArrivalsStreamName
            };

            return new EventData(
                Guid.NewGuid(),
                "registration-data",
                true,
                Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(registrationData)),
                Encoding.ASCII.GetBytes("")
                );
        }

        public void SendDepartureData(List<Flight> x)
        {
            _connection.AppendToStreamAsync(
                stream,
                ExpectedVersion.Any,
                GetEventDataFor(x)).Wait();
        }

        private void SendEmptyData()
        {
            var x = new List<Flight>();

            _connection.AppendToStreamAsync(
                stream,
                ExpectedVersion.Any,
                GetEventDataFor(x)).Wait();
        }
        private EventData GetEventDataFor(List<Flight> flights)
        {
            return new EventData(
                Guid.NewGuid(),
                "departures-table",
                true,
                Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(flights)),
                Encoding.ASCII.GetBytes("")
                );
        }

        public void Connect()
        {
            const int defaultPort = 1113;

            var settings = ConnectionSettings.Create();
            _connection = EventStoreConnection.Create(
                settings,
                new IPEndPoint(IPAddress.Loopback, defaultPort));

            _connection.ConnectAsync().Wait();
        }

        public void Register()
        {
            _connection.AppendToStreamAsync(
                RegistrationStreamName,
                ExpectedVersion.Any,
                GetRegistrationEvent()).Wait();
        }

        public void CloseConnection()
        {
            if (_connection == null)
            {
                return;
            }

            _connection.DeleteStreamAsync(stream, ExpectedVersion.Any, null).Wait();
            _connection.Close();
            _connection.Dispose();
            _connection = null;
        }

        public void SubscribeForArrivals()
        {
            _connection.SubscribeToStreamAsync(ArrivalsStreamName, true,
                (_, x) =>
                {
                    var flights = new List<Flight>();
                    var data = Encoding.ASCII.GetString(x.Event.Data);
                    var flightList = JsonConvert.DeserializeObject<List<Flight>>(data);
                    lastFlights = flightList;
                });
            SendEmptyData();
        }

    }
}
