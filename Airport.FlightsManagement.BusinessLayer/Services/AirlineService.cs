﻿using Airport.FlightsManagement.DataLayer;
using Airport.FlightsManagement.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface IAirlineService
    {
        int AddAirline(Airline airline);
        Airline GetAirlineById(int id);
        List<Airline> GetAllAirlines();
    }

    public class AirlineService : IAirlineService
    {
        private readonly Func<IAirportFlightsManagementDbContext> _dbContextFactory;

        public AirlineService(Func<IAirportFlightsManagementDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public int AddAirline(Airline airline)
        {
            using (var context = _dbContextFactory())
            {
                var newAirlineEntinty = context.Airlines.Add(airline);
                context.SaveChanges();
                return newAirlineEntinty.Entity.Id;
            }
        }

        public Airline GetAirlineById(int id)
        {
            using (var context = _dbContextFactory())
            {
                return context.Airlines.FirstOrDefault(p => p.Id == id);
            }
        }

        public List<Airline> GetAllAirlines()
        {
            using (var context = _dbContextFactory())
            {
                return context.Airlines.ToList();
            }
        }
    }
}
