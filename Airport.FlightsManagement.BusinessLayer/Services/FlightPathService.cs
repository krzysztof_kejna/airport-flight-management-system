﻿using Airport.FlightsManagement.BusinessLayer.Models;
using Airport.FlightsManagement.DataLayer;
using Airport.FlightsManagement.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface IFlightPathService
    {
        int AddFlightPath(int planeId, int airlineId, int portId, FlightPath flightPath);
        Task<List<CreatedPathFlights>> GetAllFlightPaths();
        Task<FlightPath> GetFlightPathById(int id);
        List<Flight> GetNextTwentyFlights();
    }

    public class FlightPathService : IFlightPathService
    {
        private readonly Func<IAirportFlightsManagementDbContext> _dbContextFactory;
        private readonly IAirlineService _airlineService;
        private readonly IPlaneService _planeService;
        private readonly IPortService _portService;
        private readonly SystemTimeService _systemTimeService;

        public FlightPathService(Func<IAirportFlightsManagementDbContext> dbContextFactory,
            IAirlineService airlineService,
            IPlaneService planeService,
            IPortService portService,
            SystemTimeService systemTimeService)
        {
            _dbContextFactory = dbContextFactory;
            _airlineService = airlineService;
            _planeService = planeService;
            _portService = portService;
            _systemTimeService = systemTimeService;
        }

        public int AddFlightPath(int planeId, int airlineId, int portId, FlightPath flightPath)
        {
            var choosenPlane = _planeService.GetPlaneById(planeId);
            var choosenPort = _portService.GetPortById(portId);
            var choosenAirline = _airlineService.GetAirlineById(airlineId);

            if(choosenAirline == null || choosenPlane == null || choosenPort == null)
            {
                throw new Exception("There is no one or more item with typed id");
            }

            flightPath.AirlineId = airlineId;
            flightPath.PlaneId = planeId;
            flightPath.PortId = portId;

            using (var context = _dbContextFactory())
            {
                var newFlightPathEntinty = context.FlightPaths.Add(flightPath);
                context.SaveChanges();
                return newFlightPathEntinty.Entity.Id;
            }
        }

        public async Task<List<CreatedPathFlights>> GetAllFlightPaths()
        {
            using (var context = _dbContextFactory())
            {
                return await GetValidCreatedPathFlightsItems(context.FlightPaths.AsQueryable());
            }
        }

        private async Task<List<CreatedPathFlights>> GetValidCreatedPathFlightsItems(IQueryable<FlightPath> flightPathsQueryBase)
        {
            var flightPathFlights = await flightPathsQueryBase
                .Include(f => f.Airline)
                .Include(f => f.Plane)
                .Include(f => f.Port)
                .Select(flightPath => new CreatedPathFlights
                {
                    Id = flightPath.Id,
                    AirlineName = flightPath.Airline.Name,
                    PlaneManifacturer = flightPath.Plane.Manifacturer,
                    Cost = flightPath.Cost,
                    FlightDay = flightPath.FlightDay,
                    FlightCode = flightPath.Airline.LineCode + flightPath.PathCode,
                    FlightTime = flightPath.FlightTime.Value,
                    PlaneModel = flightPath.Plane.Model,
                    PlaneSeatCount = flightPath.Plane.SeatCount,
                    PortCityName = flightPath.Port.CityName,
                    PortCountryName = flightPath.Port.CountryName,
                    PortName = flightPath.Port.Name,
                    FlightDurration = flightPath.FlightDurration
                })
                .ToListAsync();

            return flightPathFlights;
        }

        public async Task<FlightPath> GetFlightPathById(int id)
        {
            using (var context = _dbContextFactory())
            {
                return await context.FlightPaths
                    .Include(p => p.Port)
                    .FirstOrDefaultAsync(p => p.Id == id);
            }
        }

        public List<Flight> GetNextTwentyFlights()
        {
            var list = new List<Flight>();
            var flightPathList = GetAllFlightPaths();
            foreach (var path in flightPathList.Result)
            {
                var departureDate = GetNextAvalibleDay(path.FlightTime, path.FlightDay);
                list.Add(new Flight { ArrivalAirportName = path.PortName, FlightCode = path.FlightCode, DepartureAirportName = "KrzysiekAirport", DepartureDate = departureDate, ArrivalDate = departureDate.AddHours(path.FlightDurration)});
            }

            var result = list.OrderBy(f => f.DepartureDate)
                .Take(20)
                .ToList();

            return result;
        }

        private DateTime GetNextAvalibleDay(DateTime? date, int dayOfTheWeek)
        {
            int day;
            var a = (dayOfTheWeek -1) - (int)_systemTimeService.Now().DayOfWeek;
            if (a < 0)
            {
                day = a + 7;
            }
            else
            {
                day = a;
            }

            var result = new DateTime(_systemTimeService.Now().Year, _systemTimeService.Now().Month, _systemTimeService.Now().Day, date.Value.Hour, date.Value.Minute, date.Value.Minute);
            result = result.AddDays(day);

            if (result.AddMinutes(15) <= _systemTimeService.Now())
            {
                result = result.AddDays(7);
            }

            return result;
        }
    }
}
