﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface IDistanceCalculator
    {
        double CalculateDistance(double homePortLatitude, double homePortLongitude, double destinationLatidue, double destinationLongitude);
    }

    public class DistanceCalculator : IDistanceCalculator
    {
        public double CalculateDistance(double homePortLatitude, double homePortLongitude, double destinationLatidue, double destinationLongitude)
        {
            double distancePerDegree = 111.1;
            double kmForMiles = 0.621371192;
            var cosAB = (Math.Cos((90 - homePortLatitude) *Math.PI/180) * Math.Cos((90 - destinationLatidue) * Math.PI / 180)) + (Math.Sin((90 - homePortLatitude) * Math.PI / 180) * Math.Sin((90 - destinationLatidue) * Math.PI / 180) * Math.Cos(homePortLongitude * Math.PI / 180 - destinationLongitude * Math.PI / 180));
            var aCosAb = Math.Acos(cosAB) * 180/Math.PI;
            var distance = (aCosAb * distancePerDegree) * kmForMiles;
            return distance;
        }
    }
}
// Patterns for calculation above are from: https://pl.wikibooks.org/wiki/Astronomiczne_podstawy_geografii/Odległości
