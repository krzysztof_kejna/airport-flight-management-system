﻿using Airport.FlightsManagement.BusinessLayer.Models;
using Airport.FlightsManagement.DataLayer.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface IJsonConversionService
    {
        void Save(string path, List<CreatedTickets> ticketList);
    }

    public class JsonConversionService : IJsonConversionService
    {
        public void Save(string path, List<CreatedTickets> ticketList)
        {
            string json = JsonConvert.SerializeObject(ticketList);
            File.WriteAllText(path, json);
        }
    }
}
