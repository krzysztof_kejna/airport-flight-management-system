﻿using Airport.FlightsManagement.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface ITicketCostCalculationService
    {
        double TicketCostCalculation(int userId, double ticketCost);
        int CalculateAge(User user);
        double LoyalityDiscountCalculation(double userTraveledDistance);
    }

    public class TicketCostCalculationService : ITicketCostCalculationService
    {
        private readonly IUserService _userService;

        public TicketCostCalculationService(IUserService userService)
        {
            _userService = userService;
        }

        public double TicketCostCalculation(int userId, double ticketCost)
        {
            User user = _userService.GetUserById(userId);
            int age = CalculateAge(user);
            double traveledDistance = user.DistanceTraveled;

            if (age >= 15)
            {                
            }
            else if (age >= 3)
            {
                ticketCost = ticketCost * 0.5;
            }
            else
            {
                ticketCost = 0;
            }

            return ticketCost * LoyalityDiscountCalculation(traveledDistance);
        }

        public int CalculateAge(User user)
        {
            int age = 0;
            var dateTime = user.BirthDate;
            
            if(dateTime == null)
            {
                throw new Exception("This user has no birth date please edit user");
            }

            if(DateTime.Now < dateTime)
            {
                throw new Exception("This user has birth day higher than today date, please edit user");
            }

            if (DateTime.Now.Month > dateTime.Value.Month || (DateTime.Now.Month == dateTime.Value.Month && DateTime.Now.Day >= dateTime.Value.Day))
            {
                age = DateTime.Now.Year - dateTime.Value.Year;
            }
            else
            {
                age = DateTime.Now.Year - dateTime.Value.Year - 1;
            }

            if (age < 0)
            {
                age = 0;
            }

            return age;
        }

        public double LoyalityDiscountCalculation(double userTraveledDistance)
        {
            if (userTraveledDistance > 10000)
            {
                return 0.75;
            }
            else
            {
                return 1;
            }
        }
    }
}
