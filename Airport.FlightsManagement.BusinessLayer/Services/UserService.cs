﻿using Airport.FlightsManagement.DataLayer;
using Airport.FlightsManagement.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface IUserService
    {
        Task<int> AddUser(User user);
        List<User> GetAllUsers();
        void UpdateUser(User user);
        User GetUserById(int id);
        Task<bool> CheckLoginAvalibility(string login);
        Task<bool> AuthenticateUser(string login, string passwordHash);
        Task<User> GetUserByLogin(string login);
    }

    public class UserService : IUserService
    {
        private readonly Func<IAirportFlightsManagementDbContext> _dbContextFactory;

        public UserService(Func<IAirportFlightsManagementDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<int> AddUser(User user)
        {
            using (var context = _dbContextFactory())
            {
                var newUserEntinty = context.Users.Add(user);
                await context.SaveChangesAsync();
                return newUserEntinty.Entity.Id;
            }
        }

        public List<User> GetAllUsers()
        {
            using (var context = _dbContextFactory())
            {
                return context.Users.ToList();
            }
        }

        public void UpdateUser(User user)
        {
            using (var context = _dbContextFactory())
            {
                context.Users.Update(user);
                context.SaveChanges();
            }
        }

        public User GetUserById(int id)
        {
            using (var context = _dbContextFactory())
            {
                return context.Users.FirstOrDefault(p => p.Id == id);
            }
        }

        public async Task<bool> CheckLoginAvalibility(string login)
        {
            return GetAllUsers().Any(u => u.Login == login);
        }

        public async Task<User> GetUserByLogin(string login)
        {
            return GetAllUsers().FirstOrDefault(u => u.Login == login);
        }

        public async Task<bool> AuthenticateUser(string login, string passwordHash)
        {
            using (var context = _dbContextFactory())
            {
                return await context.Users.AnyAsync(x =>
                    x.Login.ToLower() == login.ToLower()
                    && x.PasswordHash.ToLower() == passwordHash.ToLower());
            }
        }
    }
}
