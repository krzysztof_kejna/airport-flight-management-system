﻿using Airport.FlightsManagement.DataLayer;
using Airport.FlightsManagement.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface IPlaneService
    {
        int AddPlane(Plane plane);
        Plane GetPlaneById(int id);
        List<Plane> GetAllPlanes();
    }

    public class PlaneService : IPlaneService
    {
        private readonly Func<IAirportFlightsManagementDbContext> _dbContextFactory;

        public PlaneService(Func<IAirportFlightsManagementDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public int AddPlane(Plane plane)
        {
            using (var context = _dbContextFactory())
            {
                var newPlaneEntinty = context.Planes.Add(plane);
                context.SaveChanges();
                return newPlaneEntinty.Entity.Id;
            }
        }

        public Plane GetPlaneById(int id)
        {
            using (var context = _dbContextFactory())
            {
                return context.Planes.FirstOrDefault(p => p.Id == id);
            }
        }

        public List<Plane> GetAllPlanes()
        {
            using (var context = _dbContextFactory())
            {
                return context.Planes.ToList();
            }
        }
    }
}
