﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Airport.FlightsManagement.BusinessLayer.Models;
using EventStore.ClientAPI;
using Newtonsoft.Json;

namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface IFlightBoardService
    {
        Task<List<Flight>> GetFlightData();
        Enum DeparturedFlightStatus(DateTime? data);
        Enum? ArrivedFlightStatus(DateTime? data);
        Task<List<Flight>> GetNext20Arrivals();
    }

    public class FlightBoardService:IFlightBoardService
    {
        private readonly IFlightPathService _flightPathService;
        private readonly ISystemTimeService _systemTimeService;

        public FlightBoardService(IFlightPathService flightPathService, ISystemTimeService systemTimeService)
        {
            _flightPathService = flightPathService;
            _systemTimeService = systemTimeService;
        }

        public async Task<List<Flight>> GetFlightData()
        {
            var flightList = _flightPathService.GetNextTwentyFlights();
            var flightInfo = new List<Flight>();
            foreach (var flight in flightList)
            {
                var departureStatus = DeparturedFlightStatus(flight.DepartureDate).ToString();
                string arrivalStatus;
                var a = ArrivedFlightStatus(flight.ArrivalDate);
                if (a == null)
                {
                    arrivalStatus = "";
                }
                else
                {
                    arrivalStatus = a.ToString();
                }

                var current = new Flight { FlightCode = flight.FlightCode, DepartureAirportName = "KrzysiekAirport", DepartureDate = flight.DepartureDate, DepartureStatus = departureStatus, ArrivalAirportName = flight.ArrivalAirportName, ArrivalDate = flight.ArrivalDate, ArrivalStatus = arrivalStatus };
                flightInfo.Add(current);
            }

            return flightInfo;
        }

        public Enum DeparturedFlightStatus(DateTime? data)
        {
            if(data.HasValue == false)
            {
                return Status.DepartureStatus.error;
            }

            var date = data - _systemTimeService.Now();
            var minutes = date.Value.TotalMinutes;

            if (minutes > 90)
            {
                return Status.DepartureStatus.awaiting;
            }

            if (minutes > 30)
            {
                return Status.DepartureStatus.checkIn;
            }

            if (minutes > 15)
            {
                return Status.DepartureStatus.checkInClosedGoToGate;
            }

            if (minutes > 0)
            {
                return Status.DepartureStatus.boarding;
            }

            if (minutes > -15)
            {
                return Status.DepartureStatus.departed;
            }

            else
            {
                return Status.DepartureStatus.error;
            }
        }

        public Enum? ArrivedFlightStatus(DateTime? data)
        {
            if (data.HasValue == false)
            {
                return Status.DepartureStatus.error;
            }

            var date = _systemTimeService.Now() - data;
            var minutes = date.Value.TotalMinutes;

            if (minutes < -15)
            {
                return null;
            }

            if (minutes <= 0)
            {
                return Status.ArrivalStatus.landing;
            }
            if (minutes <= 30)
            {
                return Status.ArrivalStatus.landed;
            }

            if (minutes <= 60)
            {
                return Status.ArrivalStatus.collectLuggage;
            }

            if (minutes > 60)
            {
                return Status.ArrivalStatus.flightProcesses;
            }

            else
            {
                return Status.ArrivalStatus.error;
            }
        }


        public async Task<List<Flight>>GetNext20Arrivals()
        {

            var flights = EventStoreConnectionService.lastFlights;
            foreach(var flight in flights)
            {
                string arrivalStatus;
                var a = ArrivedFlightStatus(flight.ArrivalDate);
                if (a == null)
                {
                    arrivalStatus = "";
                }
                else
                {
                    arrivalStatus = a.ToString();
                }

                flight.ArrivalStatus = arrivalStatus;
            }

            var results = flights.OrderByDescending(d => d.ArrivalDate)
            .Take(20)
            .ToList();

            return results;
        }

    }
}
