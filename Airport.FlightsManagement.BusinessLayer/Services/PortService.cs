﻿using Airport.FlightsManagement.DataLayer;
using Airport.FlightsManagement.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace Airport.FlightsManagement.BusinessLayer.Services
{
    public interface IPortService
    {
        int AddPort(Port port);
        List<Port> GetAllPorts();
        Port GetPortById(int id);
    }

    public class PortService : IPortService
    {
        private readonly Func<IAirportFlightsManagementDbContext> _dbContextFactory;

        public PortService(Func<IAirportFlightsManagementDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public int AddPort(Port port)
        {
            using (var context = _dbContextFactory())
            {
                var newPortEntinty = context.Ports.Add(port); 
                context.SaveChanges();
                return newPortEntinty.Entity.Id;
            }
        }

        public List<Port> GetAllPorts()
        {
            using (var context = _dbContextFactory())
            {
                return context.Ports.ToList();
            }
        }

        public Port GetPortById(int id)
        {
            using (var context = _dbContextFactory())
            {
                return context.Ports.FirstOrDefault(p => p.Id == id);
            }
        }
    }
}
