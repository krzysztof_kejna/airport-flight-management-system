using Airport.FlightsManagement.BusinessLayer.Services;
using Airport.FlightsManagement.DataLayer.Models;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;

namespace AirportFlightsManagement.BusinessLayer.Tests
{
    public class TicketCostCalculationServiceTests
    {
        [Test]
        public void UserAgeIsHigherThan15_ResultEquals100PercentOfTicketCost()
        {
            var date = new DateTime(1999, 2, 22);
            var user = new User { Name = "Jan",
                Surname = "Kowalski",
                DistanceTraveled = 1000,
                BirthDate = date};
            
            var getUserByIdMock = new Mock<IUserService>();
            getUserByIdMock.Setup(p => p.GetUserById(It.IsAny<int>())).Returns(user);
            var calculator = new TicketCostCalculationService(getUserByIdMock.Object);

            double result = calculator.TicketCostCalculation(1, 200);

            result.Should().Be(200);
        }

        [Test]
        public void UserAgeIsLowerThan15AndHigherThan3_ResultEquals50PercentOfTicketCost()
        {
            var date = new DateTime(2014, 2, 22);
            var user = new User
            {
                Name = "Jan",
                Surname = "Kowalski",
                DistanceTraveled = 1000,
                BirthDate = date
            };

            var getUserByIdMock = new Mock<IUserService>();
            getUserByIdMock.Setup(p => p.GetUserById(It.IsAny<int>())).Returns(user);
            var calculator = new TicketCostCalculationService(getUserByIdMock.Object);

            double result = calculator.TicketCostCalculation(1, 200);

            result.Should().Be(100);
        }

        [Test]
        public void UserAgeIsLowerThan3_ResultEquals0()
        {
            var date = new DateTime(2019, 2, 22);
            var user = new User
            {
                Name = "Jan",
                Surname = "Kowalski",
                DistanceTraveled = 1000,
                BirthDate = date
            };

            var getUserByIdMock = new Mock<IUserService>();
            getUserByIdMock.Setup(p => p.GetUserById(It.IsAny<int>())).Returns(user);
            var calculator = new TicketCostCalculationService(getUserByIdMock.Object);

            double result = calculator.TicketCostCalculation(1, 200);

            result.Should().Be(0);
        }

        [Test]
        public void UserAgeIsLowerThan0_ResultThrowsException()
        {
            var date = new DateTime(2021, 2, 22);
            var user = new User
            {
                Name = "Jan",
                Surname = "Kowalski",
                DistanceTraveled = 1000,
                BirthDate = date
            };

            var getUserByIdMock = new Mock<IUserService>();
            getUserByIdMock.Setup(p => p.GetUserById(It.IsAny<int>())).Returns(user);
            var calculator = new TicketCostCalculationService(getUserByIdMock.Object);

            Assert.Throws<Exception>(() => calculator.TicketCostCalculation(1, 200));
        }

        [Test]
        public void DistanceIsHigherThan10000_Returns0Point75()
        {
            var distance = 10001;
            
            var userServiceMock = new Mock<IUserService>();
            var calculator = new TicketCostCalculationService(userServiceMock.Object);
            
            var result = calculator.LoyalityDiscountCalculation(distance);
            result.Should().Be(0.75);
        }
        
        [Test]
        public void DistanceIsLowerThan10000_Returns1()
        {
            var distance = 100;
            
            var userServiceMock = new Mock<IUserService>();
            var calculator = new TicketCostCalculationService(userServiceMock.Object);
            
            var result = calculator.LoyalityDiscountCalculation(distance);
            result.Should().Be(1);
        }
    }
}