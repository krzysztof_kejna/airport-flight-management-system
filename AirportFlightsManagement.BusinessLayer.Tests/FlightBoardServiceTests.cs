﻿using Airport.FlightsManagement.BusinessLayer.Models;
using Airport.FlightsManagement.BusinessLayer.Services;
using Airport.FlightsManagement.DataLayer.Models;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AirportFlightsManagement.BusinessLayer.Tests
{
    class FlightBoardServiceTests
    {

        [Test]
        public void FlightDate2HoursBeforeNow_StatusAwaiting()
        {
            var date = new SystemTimeService().Now();
            date = date.AddHours(2);

            var flight = new Flight
            {
                ArrivalAirportName = "London",
                FlightCode = "23002",
                DepartureDate = date
            };
            var flights = new List<Flight>();
            flights.Add(flight);
            var ticketServiceMock = new Mock<IFlightPathService>();
            ticketServiceMock.Setup(flights => flights.GetNextTwentyFlights()).Returns(flights);

            var status = new FlightBoardService(ticketServiceMock.Object, new SystemTimeService());
            Assertion("23002", "London", date, "awaiting", status.GetFlightData().Result);
        }

        [Test]
        public void FlightDate1HoursBeforeNow_StatusCheckIn()
        {
            var date = new SystemTimeService().Now();
            date = date.AddHours(1);

            var flight = new Flight
            {
                ArrivalAirportName = "London",
                FlightCode = "23002",
                DepartureDate = date
            };
            var flights = new List<Flight>();
            flights.Add(flight);
            var ticketServiceMock = new Mock<IFlightPathService>();
            ticketServiceMock.Setup(flights => flights.GetNextTwentyFlights()).Returns(flights);

            var status = new FlightBoardService(ticketServiceMock.Object, new SystemTimeService());
            Assertion("23002", "London", date, "checkIn", status.GetFlightData().Result);
        }

        [Test]
        public void FlightDate20MinutesBeforeNow_StatusCheckInClosedGoToGate()
        {
            var date = new SystemTimeService().Now();
            date = date.AddMinutes(20);

            var flight = new Flight
            {
                ArrivalAirportName = "London",
                FlightCode = "23002",
                DepartureDate = date
            };
            var flights = new List<Flight>();
            flights.Add(flight);
            var ticketServiceMock = new Mock<IFlightPathService>();
            ticketServiceMock.Setup(flights => flights.GetNextTwentyFlights()).Returns(flights);

            var status = new FlightBoardService(ticketServiceMock.Object, new SystemTimeService());
            Assertion("23002", "London", date, "checkInClosedGoToGate", status.GetFlightData().Result);
        }

        [Test]
        public void FlightDate10MinutesBeforeNow_StatusBoarding()
        {
            var date = new SystemTimeService().Now();
            date = date.AddMinutes(10);

            var flight = new Flight
            {
                ArrivalAirportName = "London",
                FlightCode = "23002",
                DepartureDate = date
            };
            var flights = new List<Flight>();
            flights.Add(flight);
            var ticketServiceMock = new Mock<IFlightPathService>();
            ticketServiceMock.Setup(flights => flights.GetNextTwentyFlights()).Returns(flights);

            var status = new FlightBoardService(ticketServiceMock.Object, new SystemTimeService());
            Assertion("23002", "London", date, "boarding", status.GetFlightData().Result);
        }

        [Test]
        public void FlightDate10MinutesAfterNow_StatusDeparted()
        {
            var date = new SystemTimeService().Now();
            date = date.AddMinutes(-10);

            var flight = new Flight
            {
                ArrivalAirportName = "London",
                FlightCode = "23002",
                DepartureDate = date
            };
            var flights = new List<Flight>();
            flights.Add(flight);
            var ticketServiceMock = new Mock<IFlightPathService>();
            ticketServiceMock.Setup(flights => flights.GetNextTwentyFlights()).Returns(flights);

            var status = new FlightBoardService(ticketServiceMock.Object, new SystemTimeService());
            Assertion("23002", "London", date, "departed", status.GetFlightData().Result);
        }

        [Test]
        public void FlightDate30MinutesAfterNow_StatusNull()
        {
            var date = new SystemTimeService().Now();
            date = date.AddMinutes(-30);

            var flight = new Flight
            {
                ArrivalAirportName = "London",
                FlightCode = "23002",
                DepartureDate = date
            };
            var flights = new List<Flight>();
            flights.Add(flight);
            var ticketServiceMock = new Mock<IFlightPathService>();
            ticketServiceMock.Setup(flights => flights.GetNextTwentyFlights()).Returns(flights);

            var status = new FlightBoardService(ticketServiceMock.Object, new SystemTimeService());
            Assertion("23002", "London", date, "error", status.GetFlightData().Result);
        }

        [Test]
        public void FlightDateNull_StatusNull()
        {
            var flight = new Flight
            {
                ArrivalAirportName = "London",
                FlightCode = "23002",
                DepartureDate = null
            };
            var flights = new List<Flight>();
            flights.Add(flight);
            var ticketServiceMock = new Mock<IFlightPathService>();
            ticketServiceMock.Setup(flights => flights.GetNextTwentyFlights()).Returns(flights);

            var status = new FlightBoardService(ticketServiceMock.Object, new SystemTimeService());
            Assertion("23002", "London", null, "error", status.GetFlightData().Result);
        }

        private void Assertion(string code, string city, DateTime? date, string status, List<Flight> list)
        {
            foreach (var item in list)
            {
                var flight = new Flight { ArrivalAirportName = city, FlightCode = code, DepartureDate = date, DepartureStatus = status, ArrivalDate = new DateTime()};
                item.DepartureStatus.Should().Be(flight.DepartureStatus);
            }
        }             
    }
}
