﻿using Airport.FlightsManagement.BusinessLayer.Services;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AirportFlightsManagement.BusinessLayer.Tests
{
    class DistanceCalculatorTests
    {
        double _homeLatitude = 54.36;
        double _homeLongitude = 18.63;

        [Test]
        public void Latitude54Longitude17_Returns65()
        {

            double latitude = 54.46;
            double longitude = 17.01;

            var calculator = new DistanceCalculator();
            var result = calculator.CalculateDistance(_homeLatitude, _homeLongitude, latitude, longitude);

            result.Should().BeApproximately(65, 1);
        }

        [Test]
        public void Latitude53Longitude16_Returns93()
        {

            double latitude = 53.00;
            double longitude = 18.60;

            var calculator = new DistanceCalculator();
            var result = calculator.CalculateDistance(_homeLatitude, _homeLongitude, latitude, longitude);

            result.Should().BeApproximately(93, 3);
        }

        [Test]
        public void Latitude54Longitude18_Returns0()
        {

            double latitude = 54.36;
            double longitude = 18.63;

            var calculator = new DistanceCalculator();
            var result = calculator.CalculateDistance(_homeLatitude, _homeLongitude, latitude, longitude);

            result.Should().BeApproximately(0, 3);
        }
        
        [Test]
        public void Latitude40LongitudeMinus74_Returns4099()
        {

            double latitude = 40.70;
            double longitude = -74;

            var calculator = new DistanceCalculator();
            var result = calculator.CalculateDistance(_homeLatitude, _homeLongitude, latitude, longitude);

            result.Should().BeApproximately(4099, 3);
        }        
        
        [Test]
        public void LatitudeMinuse37Longitude144_Returns9697()
        {

            double latitude = -37.80;
            double longitude = 144.94;

            var calculator = new DistanceCalculator();
            var result = calculator.CalculateDistance(_homeLatitude, _homeLongitude, latitude, longitude);

            result.Should().BeApproximately(9694, 3);
        }        
        
        [Test]
        public void LatitudeMinus22LongitudeMinus43_Returns6462()
        {

            double latitude = -22.90;
            double longitude = -43.20;

            var calculator = new DistanceCalculator();
            var result = calculator.CalculateDistance(_homeLatitude, _homeLongitude, latitude, longitude);

            result.Should().BeApproximately(6462, 3);
        }
    }
}
